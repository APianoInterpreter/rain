import numpy as np
from mido import MidiFile
from pydub.playback import play

import lib
from lib import merge
from lib import velocity as v
from film import film

n = 480
c = int(n/2)
tc = int(c/3)
cc = int(c/2)
ccc = int(cc/2)
cccc = int(ccc/2)
t = int(n/3)
b = int(2*n)
piano = np.array([45, 60])
LFT = 15
tr = 15
trb = 20
fin = 8
acflt = 0


def right(mid):
    # P1
    notes = []
    dur = []
    for i in range(2):
        notes += ["e5", "d5+", "e5", "d5+", "e5", "b4", "d5", "c5",
                  "a4", "s", "c4", "e4", "a4",
                  "b4", "s", "e4", "g4+", "b4",
                  "c5", "s", "e4", "e5", "d5+"]

        notes += ["e5", "d5+", "e5", "b4", "d5", "c5",
                  "a4", "s", "c4", "e4", "a4",
                  "b4", "s", "e4", "c5", "b4"]
        if i == 0:
            m0r = list(notes)
            notes += ["a4"]
            dur += 8*[cc] + 3*[c, cc, cc, cc, cc] + 6*[cc] + 2*[c, cc, cc, cc,
                                                                cc]
            d_m0r = list(dur)
            dur += [n]
        else:
            notes += ["a4", "s", "b4", "c5", "d5"]
            dur += 8*[cc] + 3*[c, cc, cc, cc, cc] + 6*[cc] + 3*[c, cc, cc, cc,
                                                                cc]

    vol = v(piano, [[len(notes), 0]])
    trk = merge(notes, dur, vol)
    # track notes, vols and durs
    keys = notes
    vols = vol
    durs = dur


    notes = []
    dur = []
    for i in range(2):
        notes += ["e5", "g4", "f5", "e5",
                  "d5", "f4", "e5", "d5",
                  "c5", "e4", "d5", "c5"]
        notes += ["b4", "e4", "e4", "e5", "s", "e5", "e5", "e6", "d5+", "e5",
                  "d5+", "e5", "d5+", "e5", "d5+", "e5", "d5+"]
        notes += ["e5", "d5+", "e5", "b4", "d5", "c5",
                  "a4", "s", "c4", "e4", "a4",
                  "b4", "s", "e4", "g4+", "b4"]

        notes += ["c5", "s", "e4", "e5", "d5+",
                  "e5", "d5+", "e5", "b4", "d5", "c5",
                  "a4", "s", "c4", "e4", "a4",
                  "b4", "s", "e4", "c5", "b4"]
        if i == 0:
            m1r = list(notes)
            notes += ["a4", "s", "b4", "c5", "d5"]
        else:
            notes += ["a4", "s", "d4 c5", "f4 c5", "e4 g4 c5"]

        dur += 3*([c+cc] + 3*[cc]) + [c] + 12*[cc] + 10*[cc]
        dur += 3*([c] + 4*[cc]) + 6*[cc] + 2*([c] + 4*[cc])
        if i == 0:
            d_m1r = list(dur)
        dur += [c] + 4*[cc]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # P2
    # L5
    notes = ["f4", "a4", "c5", "f5", "e5", "e5", "d5", "b5-", "a5"]
    notes += ["a5", "g5", "f5", "e5", "d5", "c5"]
    notes += ["b4-", "a4", "b4-", "a4", "g4", "a4", "b4-"]
    notes += ["c5", "d5", "d5+", "e5", "e5", "f5", "a4"]
    dur = 2*[cc] + [n-2*cc] + [cc+ccc, ccc, c, c, cc+ccc, ccc] + 6*[cc]
    dur += [c, c, cc] + 4*[ccc-cccc] + [n, cc, cc, c+cc, cc, cc, cc]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L6
    notes = ["c5", "d5", "b4"]
    m2r = ["c5", "g5", "g4", "g5", "a4", "g5", "b4", "g5", "c5", "g5", "d5",
           "g5"]
    notes += m2r
    m3r = ["e5", "g5", "c6", "b5", "a5", "g5",
           "f5", "e5", "d5", "g5", "f5", "d5"]
    notes += m3r
    notes += m2r
    dur = [n, cc+ccc, ccc] + 3*12*[ccc]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L7
    notes = m3r
    notes += ["e5", "f5", "e5", "d5+", "e5", "b4",
              "e5", "d5+", "e5", "b4", "e5", "d5+"]
    notes += 2*["e5", "b4", "e5", "d5+"]
    notes += 3*["e5", "d5+"]
    dur = 24*[ccc] + [c+cc] + 3*[cc] + [c+cc] + 9*[cc]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L8-L11
    notes = list(m0r)
    notes += ["a4", "s", "b4", "c5", "d5"]
    notes += list(m1r)
    dur = d_m0r + [c, cc, cc, cc, cc] + d_m1r
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L11
    notes = ["a4", "s", "e4 g4 b4- c5+", "f4 a4 d5",
             "c5+ e5", "d5 f5", "g4+ d5 f5", "g4+ d5 f5", "a4 c5 e5"]
    dur = [c, n, 3*c, n, cc, cc, n, c, 3*c]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L12
    notes = ["f4 d5", "e4 c5", "d4 b4", "c4 f4+ a4", "c4 a4", "c4 a4", "e4 c5",
             "d4 b4", "c4 a4", "e4 g4 b4- c5+", "f4 a4 d5", "c5+ e5", "d5 f5"]
    dur = [n, cc, cc, n, c, c, c, c, 3*c, 3*c, n, cc, cc]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L13
    notes = 3*["d5 f5"] + ["g4 e5-", "f4 d5", "e4- c5",
                           "d4 f4 b4-", "d4 f4 a4"] + 2*["d4 f4 g4+"]
    notes += ["c4 e4 a4", "s", "e4 b4", "s", "a3", "c4", "e4", "a4",
              "c5", "e5", "d5", "c5", "b4"]
    dur = [n, c, 3*c, n, cc, cc] + 3*[n, c] + [c, n] + 9*[tc]
    vol = v(piano, [[len(notes)-9, 0]], vel_init=vol[-1])
    vol += v(piano, [[9, 0]], vel_init=piano[0])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L14
    notes = ["a4", "c5", "e5", "a5", "c6", "e6", "d6", "c6", "b5"]
    notes += ["a5", "c6", "e6", "a6", "c7", "e7", "d7", "c7", "b6"]
    notes += ["b6-", "a6", "g6+", "g6", "f6+", "f6", "e6", "d6+", "d6"]
    notes += ["c6+", "c6", "b5", "b5-", "a5", "g5+", "g5", "f5+", "f5"]
    dur = 4*9*[tc]
    vol = v(piano, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    # L15-END
    notes = list(m0r)
    notes += ["a4", "s", "b4", "c5", "d5"]
    notes += list(m1r + ["a4", "s"])
    dur = d_m0r + [c, cc, cc, cc, cc] + d_m1r + [3*c, c]
    vol = v(piano, [[len(notes)-5, 0]], vel_init=vol[-1])
    vol += v(piano, [[5, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur

    mid.tracks.append(trk)
    return mid, keys, durs, vols


def left(mid):
    # P1
    m1 = ["a2", "e3", "a3", "s"]
    m2 = ["e2", "e3", "g3+", "s"]
    notes = []
    dur = []
    for i in range(2):
        notes += ["s"] + m1 + m2 + m1 + m1 + m2 + m1
        if i == 0:
            dur += [8*cc] + 2*(3*[cc] + [cc+c]) + 3*[cc]
            dur += [c+cc+6*cc] + 3*(3*[cc] + [cc+c])
            m0l = list(notes)
            d_m0l = list(dur)
        else:
            dur += [6*cc] + 2*(3*[cc] + [cc+c]) + 3*[cc] + [c+cc+6*cc]
            dur += 3*(3*[cc] + [cc+c])
    vol = v(piano-LFT, [[len(notes), 0]])
    trk = merge(notes, dur, vol)
    # track notes, vols and durs
    keys = notes
    vols = vol
    durs = dur

    notes = []
    dur = []
    for i in range(2):
        notes += ["c3", "g3", "c4", "s"] + ["g2", "g3", "b3", "s"]
        notes += ["a2", "e3", "a3", "s"]
        notes += ["e2", "e3", "s", "s", "e4", "s", "s", "s",
                  "s", "s", "s", "s", "s"]
        notes += m1 + m2 + m1 + ["s"] + m1 + m2

        dur += 3*(3*[cc] + [c+cc])
        dur += 3*[cc] + [c] + 2*[cc] + [c] + 2*[cc] + [c] + 2*[cc] + [4*c+cc]
        dur += 3*(3*[cc] + [c+cc]) + [6*cc] + 2*(3*[cc] + [c+cc])

        if i == 0:
            m1l = list(notes)
            d_m1l = list(dur)
            notes += m1
            dur += 3*[cc] + [c+cc]
        else:
            notes += ["a2", "e3", "a3", "b3- c4", "a3 c4", "g3 b3- c4"]
            dur += 6*[cc]

    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # P2
    # L5
    m3 = ["f3", "a3", "c4", "a3", "c4", "a3"]
    notes = m3 + ["f3", "b3-", "d4", "b3-", "d4", "b3-"]
    notes += ["f3", "e4", "f3 g3 b3-", "e4", "f3 g3 b3-", "e4"]
    notes += 2*m3
    notes += ["e3", "a3", "c4", "a3", "d3 d4", "e3"]
    dur = 6*6*[cc]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L6
    notes = ["g3", "e4", "g3", "f4", "g3", "f4"]
    notes += ["c4 e4", "s", "f4 g4", "e4 g4", "d4 f4 g4"]
    notes += ["c4 e4 g4", "f3 a3", "g3 b3"]
    notes += ["c4", "s", "f4 g4", "e4 g4", "d4 f4 g4"]
    dur = 6*[cc] + [c] + 4*[cc] + 3*[c] + [c] + 4*[cc]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L7
    notes = ["c4 e4", "f3 a3", "g3 b3", "g3+ b3", "s"]
    dur = 4*[c] + [11*c]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L8-L11
    notes = m0l + m1l
    dur = d_m0l + d_m1l
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L11
    notes = 5*6*["a2"]
    dur = 5*6*[cc]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L12
    notes = 6*["d2 a2"] + 6*["d2+ a2"] + 4*["e2 a2"] + 2*["e2 g2+"] + ["a1 a2"]
    notes += (2*6+5)*["a2"]
    dur = 6*6*[cc]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L13
    notes = 6*["a2"] + 3*6*["b2-"] + 6*["b2"] + ["c3", "s", "e3 g3+",
                                                 "s", "a1", "s", "a3 c4 e4"]
    dur = 5*6*[cc] + [n, c] + [c, n] + 3*[c]
    vol = v(piano-LFT, [[len(notes)-3, 0]], vel_init=vol[-1])
    vol += v(piano-LFT, [[3, 0]], vel_init=piano[0]-LFT)
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L14
    notes = 2*["a3 c4 e4", "s", "a3 c4 e4"] + ["a3 c4 e4", "s"]
    dur = 6*[c] + [c, n+3*c]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    # L15-END
    notes = m0l + m1l + ["a1 a2", "s"]
    dur = d_m0l + d_m1l + [3*c, c]
    vol = v(piano-LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    # track notes, vols and durs
    keys += notes
    vols += vol
    durs += dur


    mid.tracks.append(trk)
    return mid, keys, durs, vols


def ib(mid):
    ib_var = 0
    ib_mean = 0
    ib_nuance = [200000, 400000]
    intprt = 6*[[8, -30000, ib_var], [16, 15000, ib_var]]
    intprt += 2*[[19, -10000, ib_var], [8, 10000, ib_var], [11, -20000, ib_var], [16, 10000, ib_var], [8, -20000, ib_var], [16, 10000, ib_var]]
    mid, ibs = lib.set_ibs(mid, ib_nuance, intprt, ib_init=300000)
    #mid, ibs = lib.set_ibs(mid, ib_nuance,[[2*(2+6*8) + 2*(6*14) + 6*75 + 3, 0, ib_var],
    #                                       [6*6 + 3, 4000, ib_var]], tks_per_bt=120)
    #mid, ibs = lib.set_ibs(mid, ib_nuance, [[(18+28)*6, ib_mean, ib_var],
    #                                        [28*6, ib_mean, ib_var],
    #                                        [31*6, ib_mean, ib_var],
    #                                        [12*6, ib_mean, ib_var],
    #                                       [9*6+4, ib_mean, ib_var]],
    #                       tks_per_bt=cc,
    #                       ib_init=300000)
    return mid, ibs


def pedal():
    # P1
    times = [8*cc, 3*c] + 2*[0, 3*c] + [3*c, 3*c] + [0, 3*c]
    times += [n + 8*cc, 3*c] + 2*[0, 3*c] + [3*c, 3*c] + [0, 3*c]

    times += [3*c, 3*c] + 2*[0, 3*c] + [0, 6*c] + [6*c, 3*c] + 2*[0, 3*c]
    times += [3*c, 3*c] + [0, 3*c]
    times += [3*c, 3*c] + 2*[0, 3*c]
    times += [0, 6*c] + [6*c, 3*c] + 2*[0, 3*c] + [3*c, 3*c] + [0, 3*c]

    # P2
    times += [3*17*c, 3*c] + 2*[0, 3*c] + [3*c, 3*c]
    times += [0, 3*c] + [3*c, 3*c] + 2*[0, 3*c] + [0, 6*c]

    # P3
    times += [6*c, 3*c] + 2*[0, 3*c]
    times += [3*c, 3*c] + [0, 3*c]
    times += [18*3*c, 6*9*tc]

    # P4
    times += [2*c, 3*c] + 2*[0, 3*c]  # Need to understand why we need a two
    times += [3*c, 3*c] + [0, 3*c] + [3*c, 3*c]
    times += 2*[0, 3*c] + [0, 6*c] + [6*c, 3*c] + 2*[0, 3*c] + [3*c, 3*c]
    times += [0, 3*c]
    return times


def song():
    """Create an Audiosegment for a given instrument"""
    lib.select_sf2("Realistic Piano")
    mid = MidiFile()
    mid.ticks_per_beat=120
    mid, keys, durs, vols = right(mid)
    mid, lkeys, ldurs, lvols = left(mid)
    p_times = pedal()
    mid = lib.pedal_t(mid, p_times)
    mid, ibs = ib(mid)
    rangec = lib.minmax_keys(lkeys+keys)
    rain = {}
    rain = lib.part2rain(ibs, lkeys, ldurs, lvols, rangec=rangec, tks_per_bt=cc, uniform=False, left=True)
    rain = lib.part2rain(ibs, keys, durs, vols, rangec=rangec, rain=rain, tks_per_bt=120, uniform=True, left=False)
    return lib.mid2aud(mid)+15, ibs


if __name__ == "__main__":
    out, rain = song()
    #play(out)
    out.export("song.wav")
    film(rain)
