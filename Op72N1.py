from mido import MidiFile
import lib
from lib import notes2trk, merge, gen_part
from lib import velocity as v
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
import os
from film import film
from shutil import copyfile
reload(lib)

if os.path.exists("../../.fluidsynth/default_sound_font.sf2"):
    os.remove("../../.fluidsynth/default_sound_font.sf2")
copyfile("Realistic Piano.sf2", "../../.fluidsynth/default_sound_font.sf2")

name = "Op72N1"
n = 480
c = int(n/2)
cc = int(c/2)
ccc = int(cc/2)
cccc = int(ccc/2)
t = int(n/3)
t6 = 80
t8 = 60
t10 = 48
t12 = 40
w = int(2*n)
ppiano = np.array([50, 60])
piano = np.array([60, 70])
mforte = np.array([70, 80])
forte = np.array([80, 90])
fforte = np.array([90, 100])
lft = 20

def right(mid):
    """The notes played by the right hand three tracks are needed"""
    #L1
    notes = ["s", "b4", "g5", "f5+", "e5", "d5+", "e5", "c5"]
    dur = [4*n, ccc, 3*n-ccc, c, c, 3*n, c+cc, cc]
    vol = v(piano, [[5, 0], [3, 1]])
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    notes2 = ["s"]
    dur2 = [3*4*n]
    vol2 = [0]
    trk = merge(notes, dur, vol)
    trk1 = merge(notes1, dur1, vol1)
    trk2 = merge(notes2, dur2, vol2)
    part = gen_part(notes, dur, vol, init=[])
    part1 = gen_part(notes1, dur1, vol1, init=[])
    part2 = gen_part(notes2, dur2, vol2, init=[])

    #L2
    notes = ["b4", "f4+", "c5 a4", "b4 g4","a4 f4+", "g4 e4", "f4+ d4+",
             "g4 e4", "f4+ d4+", "s", "b4 d4", "e4+", "f4+", "a4+ c5+"]
    dur = [n+c, c, c, c, c, c, n, n, c, n+c, n, n, n, n]
    vol = v(piano, [[1, 0], [5, -2], [1, 2], [1, 0], [2, -2], [4, 1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "b4", "s"] # Should be b
    dur1 = [9*n, 2*n, n]
    vol1 = [0, vol[11], 0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    notes2 = ["s"]
    dur2 = [3*4*n]
    vol2 = [0]
    trk2 = merge(notes2, dur2, vol2, trk=trk2)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)
    part2 = gen_part(notes2, dur2, vol2, init=part2)

    #L3
    notes = ["b4 d5", "d5", "d5", "b4","a4+", "b4", "d4+", "e4", "f4+"]
    dur = [2*n, n, n, 2*n, 2*n, n, n, n, n]
    vol = v(piano, [[2, 1], [3, -1], [1, 0], [3, 1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "b4", "c5+", "s"]
    dur1 = [2*n, 2*n-5, 4*n+5, 4*n]
    vol1 = [0, vol[1], vol[2], 0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    notes2 = ["s"]
    dur2 = [3*4*n]
    vol2 = [0]
    trk2 = merge(notes2, dur2, vol2, trk=trk2)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)
    part2 = gen_part(notes2, dur2, vol2, init=part2)


    #L4
    notes = ["g4 g5", "f4+ f5+", "e4 e5", "d4+ d5+", "e4 e5", "f4+ f5+", "f4+ f5+", "g4 g5", "a4 a5",
             "b4 b5", "b4 a4", "a4", "g4+", "g4", "b4 g4", "a4 f4+", "g4 e4",
             "f4+ d4","g4 e4", "g4 e5", "g4 e5", "f4+ d5+"]
    dur = [3*n, c, c, 2*n+t, t, t, t, t, t, n+c, c] + [t]*6 + [n] + [t]*3 + [2*n]
    vol = v(forte, [[4, 0], [6, 1], [7, 0], [5, -1]])
    # Add glissendos
    gl = [[0, rd.randint(0, 25)], [14, rd.randint(0, 25)]]
    trk = merge(notes, dur, vol, gl=gl, trk=trk)
    notes1 = ["s", "b4", "s"]
    dur1 = [10*n, n, 5*n]
    vol1 = [0, vol[3], 0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    notes2 = ["s"]
    dur2 = [3*4*n]
    vol2 = [0]
    trk2 = merge(notes2, dur2, vol2, trk=trk2)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)
    part2 = gen_part(notes2, dur2, vol2, init=part2)

    #L5
    notes = ["d5", "d5", "d5", "c5", "b4", "a4"] + 4*["g4"] + 6*["a4"]
    dur = [n, n, n, n+c, c, c, c] + 3*[n, c, c]
    vol = v(piano, [[3, 1], [7, -1], [6, 1]])
    vol[3] += 5
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["f4+", "g4", "f4", "e4", "f4+", "e4", "s", "e4", "s", "f4"]
    dur1 = [n, c, c, 2*n, 2*n, n, n+5, n+t-5, 2*t, 2*n]
    sel_vol = [0, 1, 1, 2, 3, 7, 7, 10, 11, 13]
    vol1 = [vol[i] for i in sel_vol]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    notes2 = ["s", "g4", "s"]
    dur2 = [6*n, 2*n, 2*n+5]
    vol2 = [0, vol[10], 0]
    trk2 = merge(notes2, dur2, vol2, trk=trk2)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)
    part2 = gen_part(notes2, dur2, vol2, init=part2)

    #L6
    notes = ["b4"]*4 + ["c5+", "b4 d5", "b4", "a4+", "a4 d5+", "e5", "b4"]
    dur = [n, c, c, n, n, 2*n, n, n, 2*n, n, n]
    vol = v(piano, [[5, -1]], vel_init=vol[-1])
    vol += v(mforte, [[1, -1], [1, 0], [1, 1], [3, -4]], vel_init=vol[-1])
    vol[8] += 8
    notes1 = ["f4+", "s", "g4 b4", "f4+", "s", "c5+", "a4", "g4"]
    dur1 = [n+t, 2*t, 2*n-5, n+t+5, 2*t, 2*n, 2*n, 2*n-5]  # Avoid interference?
    vol1 = [vol[0], 0, vol[4], 0, vol[5], vol[6], vol[8], vol[9]]
    notes2 = ["a4", "s"]
    dur2 = [2*n, 2*n+2*4*n]
    vol2 = [vol[0], 0]
    trk = merge(notes, dur, vol, trk=trk)
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    trk2 = merge(notes2, dur2, vol2, trk=trk2)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)
    part2 = gen_part(notes2, dur2, vol2, init=part2)


    #L7
    notes = ["g4", "f4+", "e4", "d4", "c4+", "e4", "d4+ b4", "s"]
    dur = [n, c, c, 2*n, 3*n, n, 2*n, 2*n]
    vol = v(np.array([30, 60]), [[8, -5]], vel_init=vol[-1])
    notes1 = ["a4+", "b4", "b4", "a4+", "b4", "s"]
    dur1 = [2*n+5] + [2*n]*5
    vol1 = [vol[0], vol[3], vol[4], vol[5], vol[6], 0]
    trk = merge(notes, dur, vol, trk=trk)
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)
    part2 = gen_part(notes2, dur2, vol2, init=part2)

    #L8
    notes = ["b4 d5+", "a4 c5+", "a4 c5+", "g4+ b4", "g4+ b4", "a4 c5", "c5+"]
    notes += ["b4 d5+", "c5+ e5", "d5+ f5+", "e5 g5", "e5 g5"]
    dur = [2*n, ccc, 2*n-ccc, ccc, 3*n-ccc, n, 2*n-c, c, c, c, c, c]
    vol = v(piano, [[5, 0], [7, 1]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "a4+ c5+", "a4+"]
    dur1 = [8*n, 3*n, n]
    vol1 = [0, vol[5], vol[9]]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L9
    notes = ["e5 g5", "d5+ f5+", "s", "d4+ a4 c5+", "d4+ a4 b4", "c5+", "b4", "b4"]
    dur = [n, 3*n, 2*n, n+c, c, n, 2*n, n]
    vol = v(piano, [[2, -1]])
    vol += v(mforte, [[6, 0]])
    gliss = rd.randint(25)
    trk = merge(notes, dur, vol, gl=[[0, gliss]], trk=trk)
    notes1 = ["b4", "d4+ b4 d5+", "s", "e4 g4+"]
    dur1 = [4*n, 2*n-5, 2*n+5, 4*n-5]
    vol1 = [vol[0], vol[0], 0, vol[5]]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L10
    notes = ["e4 g4", "d4+ f4+"]*3 + ["e4", "d4+", "s", "b4", "b4", "g5", "b4", "c5", "b4", "a4+", "b4", "e5"]
    dur = [n+c] + [c]*4 + [c] + [n, 2*n+c, cc, cc, ccc, 3*n-ccc, cccc, cccc, cc-cccc, cc-cccc, cc, cc]
    vol = v(forte, [[7, 0], [3, 1], [8, 0]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["a4+", "f4+ a4", "s"]
    dur1 = [4*n+5, 3*n+c, 2*cc+4*n]
    vol1 = [vol[0], vol[6], 0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L11
    notes = ["d5+", "e5", "f5+", "e5", "d5+", "e5", "f5+", "g5", "a5"]
    notes += ["b5", "b5", "c6", "b5", "c6+", "d6+", "e6", "f6+", "g6"]
    notes += ["d5+", "f6+", "e6", "c6+", "a5+", "g5", "e5", "c5+", "a4+", "e4", "d4+ b4", "s"]
    dur = ([3*n] + 2*[cccc] + [t6-2*cccc] + [t6]*5)*2 + [ccc, n-ccc] + 8*[t8] + 2*[n]
    vol = v(forte, [[9, 1], [2, 0], [7, 1], [2, 0], [10, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, gl=[[28, rd.randint(25)]], trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L12
    notes = ["d4", "e4", "f4+", "g4", "a4"] + 4*["b4"]
    notes += ["a4+", "g4+", "a4+",
              "b4", "c5+", "d5",
              "e5", "f5", "g5+", "a5+"]
    notes += ["b5"] + ["c6+", "b5"]*8
    dur = [n] + 4*[cccc] + [t-cccc, t-cccc, t-cccc, n-cccc] + 10*[t10] + [2*n] + [ccc, ccc]*8
    vol = v(piano,[[9, 0], [10, 1], [1, 0], [2*8, 1]], vel_init = vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [2*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)


    #L13
    notes = ["a5", "b5", "c6+"] + ["d6", "c6+"]*8 + ["c6+", "a5+"]*4
    notes += ["d6", "c6+", "b5+", "c6+", "d6", "d6+", "e6", "e6+", "f6+", "g6", "g6+", "a6", "a6+"]
    notes += ["b5 b6", "d4+", "e4", "f4+", "g4 g5", "f4+ f5+", "e4 e5"]
    dur = 3*[cccc] + [ccc-cccc]*3 + [ccc]*(5+16)  + 12*[37] + [36] + 4*[n] + [3*n, c, c]
    vol = v(piano,[[3+2*12+13, 1]],vel_init = vol[-1])
    vol += v(forte,[[4, 1], [3, 0]],vel_init = vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "a5+"]
    dur1 = [3*n, n]
    vol1 = [0, vol[5]]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L14
    notes = ["d4+ d5+", "e4 e5", "f4+ f5+", "g4 g5", "a4 a5", "s", "b4 b5"]
    notes += 4*["b4 b5"] + ["b4", "b4 a4", "b4 a4", "b4 g4+", "b4 g4", "b4 g4", "a4 f4+", "g4 e4"]
    notes += ["d4+ f4+", "e4 g4", "g4 e5", "g4 e5", "f4+ d5+"]
    dur = [2*n+t, t, t, t, t, cc, t-cc]
    dur += [t]*12 + [n] + 3*[t] + [2*n]
    vol = v(forte,[[1, 0], [6, 1]], vel_init = vol[-1])
    vol += v(fforte,[[6, 0], [6, -1], [4, 0], [1, -1]],vel_init = vol[-1])
    # Add glissendos
    gl = [[10, rd.randint(0, 25)], [16, rd.randint(0, 25)]]
    # Add accent
    vol[11] = min(vol[11] + 5, 127)
    vol[12] = min(vol[12] + 5, 127)
    vol[13] = min(vol[13] + 5, 127)
    vol[16] = min(vol[16] + 5, 127)
    trk = merge(notes, dur, vol, gl=gl, trk=trk)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L15
    notes = 3*["d5+"] + 3*["e5"] + ["g5", "f5+", "e5", "e5", "d5+"]
    dur = 2*[2*n, n, n] +  [n+c+cc, ccc, ccc, n, n]
    vol = v(forte, [[4, -1], [2, 1], [5, -1]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "f4+ a4", "f4+ a4"] + 3*["e4 g4"] + ["g4 b4", "f4+ a4 b4"]
    dur1 = [5*4*n, 2*n] + 2*[2*n] + [n, n, 2*n, 2*n]
    v_sel = [0, 0, 1, 3, 4, 5, 6, 9]
    vol1 = [vol[i] for i in v_sel]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L16
    notes = ["e4 g4+ b4 d5", "s", "e5 g5+", "d5 f5+", "c5+ e5", "c5+ e5", "d5 f5"]
    dur = [3*n, n, 2*n, 2*n, ccc, 3*n-ccc, n]
    vol = v(piano, [[2, -1]])
    vol += v(ppiano, [[5, -1]])
    gl = [[0, rd.randint(0, 25)]]
    trk = merge(notes, dur, vol, gl=gl, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    #L17
    notes = ["f5+", "e5 g5+", "f5+ a5", "g5+ b5", "a5 c6", "a5 c6", "a5 c6"]
    notes += ["b5 g5+", "s"] + 4*["g5+ e5"] + ["g5", "f5+"] + 3*["d5 f5+"]
    dur = [n+c] + 5*[c] + [n, n, 2*n, n] + 3*[t] + 2*[c] + 3*[t]
    vol = v(ppiano, [[6, 1]], vel_init=vol[-1])
    vol += v(piano, [[12, 0]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "d5+", "d5+", "e5", "s", "g4+", "d5+", "d5+", "g4+"]
    dur1 = [3*4*n, 3*n, n, 2*n, 2*n, 2*n, ccc, n-ccc, n]
    v_sel = [0, 0, 4, 6, 8, 10, 13, 13, 15]
    vol1 = [vol[i] for i in v_sel]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L18
    notes = ["f5+", "a4 c5+ e5", "s"] + ["a4 c5", "g4+ b4"]*3 + ["g4+ b4 e4", "s"]
    dur = [ccc, 2*n-ccc, 2*n] + [n+c] + 5*[c] + 2*[2*n]
    vol = v(piano, [[4, 0], [7, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "d5+" , "s"]
    dur1 = [4*n]*3
    v_sel = [0, 3 ,0]
    vol1 = [vol[i] for i in v_sel]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L19
    notes = ["d4+ f4+ a4", "d4+ f4+ a4", "e4 g4+", "s", "e2", "s"]
    dur = [3*n, n, n, 3*n, 3*n, n]
    vol = v(piano, [[3, 0]], vel_init=vol[-1])
    vol += v(ppiano, [[3, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    for tr in [trk, trk1]:
        mid.tracks.append(tr)

    return mid, part, part1, part2


def left(mid):
    #L1
    notes = ["e2", "b2", "g3", "e3", "c4", "b3"]*4
    notes += ["f2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["g2", "b2", "g3", "e3", "c4", "b3"]
    dur = [t]*36
    vol = v(piano-lft, [[24, 0], [12, 1]])
    trk = merge(notes, dur, vol)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1)
    part = gen_part(notes, dur, vol, init=[])
    part1 = gen_part(notes1, dur1, vol1, init=[])


    #L2
    notes = ["d2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["e2", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["b1", "b2", "b3", "a3+", "g3", "a2+"]
    notes += ["b2", "b1", "b2", "d3+", "f3+", "b3"]
    notes += ["g2", "d3", "b3", "g3", "e4", "d4"]
    notes += ["f2+", "f3+", "c4+", "e4", "c4+", "f3+"]
    dur = [t]*36
    vol = lib.velocity(piano-lft, [[4, 0], [8, -1], [4, 1], [2, -1], [18, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L3
    notes = ["b2", "f3+", "d3", "b3", "g4", "f4+"]
    notes += ["e4+", "d4", "b3", "g3+", "e3+", "e2+"]
    notes += ["f2+", "f3+", "c4+", "e4", "g4", "f4+"]
    notes += ["e4", "c4", "a3+", "f3+", "f2+", "f1+"]
    notes += ["b1", "b2", "f3+", "s", "c4", "b3"]
    notes += ["s", "c4", "b3", "s", "c4", "b3"]
    dur = [t]*36
    vol = lib.velocity(piano-lft, [[6, 1], [18, -1], [3, 0], [9, 1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "a3", "g3", "f3+"]
    dur1 = [2*4*n + n, n, n, n]
    vol1 = [0, vol[19], vol[22], vol[25]]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L4
    notes = ["e2", "b2", "g3", "e3", "c4", "b3"]*2
    notes += ["f2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["g2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["d2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["e2", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["b1", "b2", "b3", "a3+", "g3", "a2+"]
    notes += ["b2", "b1", "b2", "d3+", "f3+", "b3"]
    dur = [t]*48
    vol = v(mforte-lft, [[18, 0], [7, 1], [23, 0]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [4*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L5
    notes = ["b2", "b1", "b2", "d3", "g3", "b3"]
    notes += ["c4", "b3", "c4", "g3", "e3", "c3"]
    notes += ["g2", "d3", "g3", "b3", "e4", "d4"]
    notes += ["c4", "c3", "e3", "g3", "c4", "e4"]
    notes += ["c4+", "c3+", "e3", "a3", "f4", "e4"]
    notes += ["d2", "d3", "f3", "a3", "e4", "d4"]
    dur = [t]*36
    vol = v(piano-lft, [[7, 1], [12, -1], [5, 0], [12, 1]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)


    #L6
    notes = ["d2+", "d3+", "g3", "b3", "g4", "f4+"]
    notes += ["e2", "e3", "g4", "b3", "g4", "e4"]
    notes += ["f2+", "f3+", "d3", "b3", "g4", "f4+"]
    notes += ["f2+", "f3+", "d3", "c4+", "g4", "f4+"]
    notes += ["b1", "b2", "f3+", "b3", "g4", "f4+"]
    notes += ["e2", "b2", "f3+", "b3", "g4", "f4+"]
    dur = [t]*36
    vol = v(piano-lft, [[12, -1]], vel_init=vol[-1])
    vol += v(mforte-lft, [[6, -1], [2, 0], [4, 1], [12, -1]])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L7
    notes = ["f2+", "c3+", "e3", "a3+", "d4", "c4+"]
    notes += ["f2+", "d3", "f3+", "b3", "a3", "b3"]
    notes += ["f2+", "c3+", "e3+", "f3+", "a3", "g3"]
    notes += ["f2+", "c3+", "e3+", "f3+", "e3+", "f3+"]
    notes += ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    dur = [t]*36
    vol = v(np.array([30, 60])-lft, [[36, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L8
    notes = ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "b2", "g3+", "e3", "c3+", "b2"]
    notes += ["b1", "b2", "g3+", "e3", "c3+", "b2"]
    notes += ["b1", "b2", "g3", "e3", "c3+", "b2"]
    notes += ["b1", "b2", "g3", "e3", "c3+", "b2"]
    dur = [t]*36
    vol = v(piano-lft, [[18, 0], [18, 1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L9
    notes = ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "f2+", "f3+", "d3+", "c3+", "b2"]
    notes += ["b1", "b2", "g3+", "e3", "c3+", "b2"]
    notes += ["b1", "b2", "g3+", "e3", "c3+", "b2"]
    dur = [t]*36
    vol = v(piano-lft, [[4, -1], [2, 0], [6, 1]], vel_init=vol[-1])
    vol += v(mforte-lft, [[24, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L10
    notes = ["b1", "b2", "g3", "e3", "c3+", "b2"]
    notes += ["b1", "b2", "g3", "e3", "c3+", "b2"]
    notes += ["b1", "b2", "f3+", "a3", "c4", "b3"]
    notes += ["a3", "f3+", "d3+", "c3", "b2", "b1"]
    notes += ["e2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["e2", "b2", "g3", "e3", "c4", "b3"]
    dur = [t]*36
    vol = v(forte-lft, [[13, 0], [11, 1], [12, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L11
    notes = ["f2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["g2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["d2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["e2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["b1", "b2", "f3+", "g3", "b2", "a3+"]
    notes += ["b3", "b2", "a2+", "b2", "d3+", "f3+"]
    dur = [t]*36
    vol = v(forte-lft, [[12, 1], [9, 0], [3, 1], [1, 0], [11, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [3*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L12
    notes = ["g2", "d3", "b3", "g3", "e4", "d4"]
    notes += ["f2+", "f3+", "d4", "e4", "c4+", "f3+"]
    notes += ["b2", "f3+", "d4", "b3", "g4", "f4+"]
    notes += ["e4+", "d4", "b3", "e3+", "d4", "e2+"]
    dur = [t]*6*4
    vol = v(piano-lft, [[6, 0], [6, 1], [6, 0], [6, 1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s"]
    dur1 = [2*4*n]
    vol1 = [0]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L13
    notes = ["f2+", "f3+", "c4+", "e4", "g4", "f4+"]
    notes += ["e4", "c4+", "a3+", "f3+", "f2+", "f1+"]
    notes += ["b2", "b3", "f3+", "s", "c4", "b3"]
    notes += ["s", "c4", "b3", "s", "c4", "b3"]
    notes += ["e2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["e2", "b2", "g3", "e3", "c4", "b3"]
    dur = [t]*6*6
    vol = v(piano-lft, [[12, 1]], vel_init=vol[-1])
    vol += v(forte-lft, [[3, 0], [9, 1], [12, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    notes1 = ["s", "a3", "g3", "f3+"]
    dur1 = [5*n, n, n, n]
    vol1 = [0, vol[15], vol[18], vol[21]]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    #L14
    notes = ["f2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["g2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["d2+", "b2", "a3", "f3+", "c4", "b3"]
    notes += ["e2", "b2", "g3", "e3", "c4", "b3"]
    notes += ["b1", "b2", "b3", "a3+", "b2", "b1"]
    notes += ["b2", "b1", "b2", "d3+", "f3+", "b3"]
    dur = [t]*6*6
    vol = v(forte-lft, [[6, 0], [6, 1]], vel_init=vol[-1])
    vol += v(fforte-lft, [[24, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    #L15
    notes = ["c4", "c3", "b2", "c3", "d3", "c3"]
    notes += ["b2", "b1", "b2", "f3+", "c4", "b3"]
    notes += ["b2", "b1", "b2", "e3", "c4", "b3"]
    notes += ["a3+", "g3", "e3", "a2+", "g2", "a1+"]
    notes += ["a1+", "b2", "e3", "g3", "c4", "b3"]
    notes += ["b2", "a2+", "b2", "b1", "a1+", "b1"]
    dur = [t]*6*6
    vol = v(forte-lft, [[13, -1], [2, 0], [3, 1], [3, -1], [3, 0], [12, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    #L16
    notes = ["e1", "e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "e3", "c4+", "a3", "f3+", "e3"]
    notes += ["e2", "e3", "c4+", "a3", "f3+", "e3"]
    dur = [ccc, t-ccc] + 5*[t] + [t]*5*6
    vol = v(piano-lft, [[13, -1]], vel_init=vol[-1])
    vol += v(piano-lft, [[24, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    #L17
    notes = ["e2", "e3", "c4", "a3", "f3+", "e3"]
    notes += ["e2", "e3", "c4", "a3", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    dur = [t]*6*6
    vol = v(piano-lft, [[6, 1]], vel_init=vol[-1])
    vol += v(piano-lft, [[18, 1], [12, 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    #L18
    notes = ["e2", "e3", "c4+", "a3", "f3+", "e3"]
    notes += ["e2", "e3", "c4+", "a3", "f3+", "e3"]
    notes += ["e2", "e3", "c4", "a3", "f3+", "e3"]
    notes += ["e2", "e3", "c4", "a3", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "b3", "g3+", "f3+", "e3"]
    dur = [t]*6*6
    vol = v(piano-lft, [[24, 0], [12, -1]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)
    part = gen_part(notes, dur, vol, init=part)

    #L19
    notes = ["e2", "e3", "s", "a3", "f3+", "e3"]
    notes += ["e2", "e3", "s", "a3", "f3+", "e3"]
    notes += ["e2", "b2", "s", "g3+", "f3+", "e3"]
    notes += ["e2", "b2", "s", "g3+", "f3+", "e3"]
    notes += ["e3 g3+ b3 e4", "s"]
    dur = [t]*6*4 + [2*n, n]
    vol = v(piano-lft, [[12, -1]], vel_init=vol[-1])
    vol += v(ppiano-lft, [[14, 0]], vel_init=vol[-1])
    gl = [[24, rd.randint(25)]]
    trk = merge(notes, dur, vol, gl=gl, trk=trk)
    notes1 = ["s", "c4", "s", "c4", "s", "b3", "s", "b3"]
    dur1 = [4*4*n+12*4*n+2*t, n, 3*t, n, 3*t, n, 3*t, n]
    vol1 = [0, vol[2], 0, vol[8], 0, vol[14], 0, vol[20]]
    trk1 = merge(notes1, dur1, vol1, trk=trk1)
    part = gen_part(notes, dur, vol, init=part)
    part1 = gen_part(notes1, dur1, vol1, init=part1)

    for tr in [trk, trk1]:
        mid.tracks.append(tr)
    return mid, part, part1


def tempo(mid):
    VAR = 10000
    TP_MIN = 800000
    TP_MAX = 1200000
    ATEMPO = int((TP_MIN+TP_MAX)/2)

    beats = lib.rand_ev(TP_MIN, TP_MAX, 4*8+1, seed=ATEMPO, scale=VAR)
    beats += lib.rand_ev(TP_MIN, TP_MAX, 3, seed=beats[-1], scale=VAR, loc=90000)
    beats += lib.rand_ev(TP_MIN, TP_MAX, 4*20, seed=beats[-1]-3*90000, scale=VAR)
    beats += lib.rand_ev(TP_MIN, TP_MAX, 4, seed=beats[-1], scale=VAR, loc=90000)
    beats += lib.rand_ev(TP_MIN, TP_MAX, 4*25, seed=ATEMPO, scale=VAR)
    beats += lib.rand_ev(TP_MIN, TP_MAX, 4+1, seed=beats[-1], scale=VAR, loc=1100000)

    trk_tempo = lib.tempo(beats)
    mid.tracks.append(trk_tempo)
    return mid, beats


def pedal():
    #P1
    times = 6*[0, 2*n]
    times += 2*[0, 2*n] + [0, n-t] + [n+2*t, 2*n-t] + 2*[0, 2*n]
    times += 4*[0, 2*n] + [0, n]
    times += [n, 2*n] + 5*[0, n] + [n, n] + [0, n] + [0, 2*n]
    #P2
    times += [0, 2*n] + [n, n] + [0, 2*n]*7 + [0, n] + [n, 2*n] + [0, 2*n]
    #L7
    times += [0, 2*n]*3 + [0, n] + [n, 4*n]
    #L8
    times += [0, 4*n]*2 + [0, 2*n]*2
    #L9
    times += [0, 4*n] + [0, 2*n]*4
    #P3
    #L10
    times += [0, 2*n]*6
    #L11
    times += [0, 2*n]*4 + [0, n]*3
    #L12
    times += [0, 2*n]*4
    #L13
    times += [0, 2*n]*3 + [2*n, 2*n] + [0, 2*n]
    #L14
    times += [0, 2*n]*3 + [0, n] + [2*n, n] + [0, n] + [0, 2*n]
    #P4
    #L15 - 19
    times += [2*n, 2*n] + 5*[0, 2*n]
    times += [0, 4*n] + [0, 4*n]
    times += [0, 2*n]*2 + 2*[0, 4*n] + [0, 4*n]
    times += [0, 4*n] + 2*[0, 2*n] + [0, 4*n]
    times += 2*[0, 4*n]
    return times


def song(name):
    """Create the Audiosegment"""
    mid = MidiFile()
    mid.ticks_per_beat = n
    mid, part, part1, part2 = right(mid)
    mid, lpart, lpart1 = left(mid)
    p_times = pedal()
    mid = lib.pedal_t(mid, p_times)
    mid, beats = tempo(mid)
    rain = {}
    rain = lib.part2rain(beats, lpart , tks_per_bt=480, rain=rain, distrib='left')
    rain = lib.part2rain(beats, lpart1 , tks_per_bt=480, rain=rain, distrib='left')
    rain = lib.part2rain(beats, part , tks_per_bt=480, rain=rain, distrib='right')
    rain = lib.part2rain(beats, part1 , tks_per_bt=480, rain=rain, distrib='right')
    rain = lib.part2rain(beats, part2 , tks_per_bt=480, rain=rain, distrib='right')
    mid.save("test.mid")
    return lib.mid2aud(mid), rain


#out = AudioSegment.silent(duration=2000)
out, rain = song(name)
out.export("song.wav")
#film(rain)
play(out)
