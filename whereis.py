from mido import MidiFile
import lib
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
import os
from shutil import copyfile
from film import film
reload(lib)

if os.path.exists("../../.fluidsynth/default_sound_font.sf2"):
    os.remove("../../.fluidsynth/default_sound_font.sf2")
copyfile("Realistic Piano.sf2", "../../.fluidsynth/default_sound_font.sf2")

name = "whereis"
n = 480
c = 240
cc = 120
ccc = 90
cccc = 60
piano = np.array([50, 70])
forte = np.array([70, 90])
ff = np.array([90, 110])
lft = 5

def right_main(mid):
    """Assemble all the part from the right hand and add it to the midfile"""
    notes = 12*["e5", "g5+"] + ["d5+", "g5+", "d5+", "g5+", "d5+", "e5", "e5", "d5+", "e5", "g5+"]
    duration = 12*[c, c] + [c, c, c, cc, cc, c, cc, cc, c, c]

    notes += 4*["e5", "g5+"] + ["d5+", "g5+", "d5+", "g5+", "d5+", "e5", "e5", "d5+", "e5", "e4"]
    duration += 4*[c, c] + [c, c, c, cc, cc, c, cc, cc, c+cc, cc]
    notes += ["g4+", "g4+", "e4", "e4", "d4+", "d4+", "g3+", "s", "c4+", "d4+", "e4"]
    duration += [c, cc, 2*c, cc, c, cc, 2*n+cc, 2*n, cc, cc, c]

    notes += ["g4+", "g4+", "e4", "e4", "d4+", "d4+", "c4+", "d4+", "s", "d4+"]
    duration += [c, cc, 2*c, cc, c, cc, c+cc, c+2*n, n+c+cc, cc]
    notes += ["e4", "e4", "d4+", "d4+", "c4+", "c4+", "d4+", "d4+"]
    duration += [c, cc, 2*cc+c, cc, c, cc, c+2*cc, cc]
    notes += ["g4+", "f4+", "e4", "c4+", "g4+", "f4+", "e4"]
    duration += [c, cc, 2*cc+c, cc, c, c, 2*n]

    notes += ["s", "g5+", "g5+", "c5+", "s", "g5+", "g5+", "c5+", "a5", "s", "g5+", "g5+", "c5+"]
    duration += [c, c+cc, c+cc, 2*n, c, c+cc, c+cc, n, n, c, c+cc, c+cc, 2*n]
    notes += ["s", "a3 b3+ e4", "s", "b5 e6 g6+ b6", "s", "e4"]
    duration += [5*n, 3*n, 2*n, n, c+cc, cc]

    notes += ["g4+", "g4+", "e4", "e4", "d4+", "d4+", "g3+", "s", "c4+", "d4+", "e4"]
    duration += [c, cc, 2*c, cc, c, cc, 2*n+cc, 2*n, cc, cc, c]
    notes += ["g4+", "g4+", "e4", "e4", "d4+", "d4+", "c4+", "d4+", "s", "d4+"]
    duration += [c, cc, 2*c, cc, c, cc, c+cc, c+2*n, n+c+cc, cc]

    notes += ["e4", "d4+", "d4+", "c4+", "c4+", "d4+", "c4+"]
    duration += [c+cc, 2*c, cc, c, cc, 2*c, cc]
    notes += ["g4+", "f4+", "e4", "c4+", "g4+", "f4+", "e4"]
    duration += [c, cc, cc+c, c, c, c, 2*n]
    notes += ["s", "g5+", "g5+", "c5+", "s", "g5+", "g5+", "c5+", "a5"]
    duration += [c, c+cc, c+cc, 2*n, c, c+cc, c+cc, c+cc, n+cc]

    notes += ["s", "g5+", "g5+", "c5+", "s", "b5", "a5", "g5+", "f5+", "g5+", "g5+"]
    duration += [c, c+cc, c+cc, 2*n, 7*n, n+c, cccc, cccc, cc, n+c+cc, cc]

    notes += ["f5+", "f5+", "e5", "d5+", "c5+", "d5+", "e5", "f5+", "g5+", "a5"]
    duration += [n+c+cc] + 9*[cc]
    notes += ["b5", "a5", "g5+", "f5+", "g5+", "g5+"]
    duration += [n+c, cccc, cccc, cc, n+c+cc, cc]
    notes += ["f5+", "f5+", "e5", "f5+", "g5+", "a5", "b5", "c6+", "d6+"]
    duration += [n+c+cc, cc, cc, cc, cc, cc, cc, cc, c+n]

    notes += ["e5 e6", "d5+ d6+", "s", "e5 e6", "d5+ d6+", "s"]
    duration += [n, 4*n, 3*n, n, 4*n, 4*n]

    notes += 4*["e5", "g5+"] + ["d5+", "g5+", "d5+", "s", "d5+", "e5", "e5", "d5+", "e5", "g5+"]
    duration += 4*[c, c] + [c, c, c, cc, cc, c, cc, cc, c, c]

    notes += 4*["e5", "g5+"] + ["d5+", "g5+", "d5+", "g5+", "d5+", "e5", "e5", "d5+", "e5", "g5+"]
    duration += 4*[c, c] + [c, c, c, cc, cc, c, cc, cc, c, c]

    notes += 4*["e5", "g5+"] + ["d5+", "g5+", "d5+", "g5+", "d5+", "e5", "e5", "d5+", "e5", "f5+", "g5+"]
    duration += 4*[c, c] + [c, c, c, cc, cc, c, cc, cc, c, c, 2*n]

    volume = lib.velocity(piano, [[16, 1], [31, 0], [8, 1], [1, -1], [6, 1],
                                  [6, -1], [9, 1], [7, 0],
                                  [12, 0], [2,-1], [6, 0]])
    volume += lib.velocity(forte, [[5, 0], [3, -1], [3, 1], [7, 0], [3, -1],
                                   [3, 1], [7 , 0], [4, -1]])
    volume += lib.velocity(forte, [[10, 0], [4, 0], [2, -1]])
    volume += lib.velocity(forte, [[7, 0],
                                   [17, 0], [7, 1]])
    volume += lib.velocity(ff, [[6, 0]])
    volume += lib.velocity(piano, [[40+5, 0], [11, -1]])

    out = [[notes[i], duration[i], volume[i]] for i in range(len(notes))]

    out[104] = out[104] + [100, 10]

    trk = lib.notes2trk(out)
    mid.tracks.append(trk)
    return mid, out[204][2], out


def right(mid):
    mid, vol, rl1 = right_main(mid)
    out = [["s", 39*4*n+c, 0], ["g5+", c, vol]]
    trk = lib.notes2trk(out)
    mid.tracks.append(trk)
    return mid, rl1, out


def left(mid):
    """Assemble all the part from the right hand and add it to the midfile"""
    notes = ["e2"] + ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    duration = [8*n] + [n]*8

    notes +=  ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    notes +=  ["e2", "e3 g3+ b3", "c2+", "c3+ e3", "g2+", "d3+ g3+ b3+", "a2", "e3 a3"]
    duration += [n]*16

    notes += ["e2", "e3 g3+ b3", "c2+", "c3+ e3 g3+", "g2+", "d3+ g3+ b3+", "a2", "e3 a3 c4+"]
    notes += ["e2", "e3 g3+ b3", "c2+", "c3+ e3 g3+", "g2+", "d3+ g3+ b3+", "a2", "e3 a3 c4+"]
    duration += [n]*15 + [2*n]

    notes += ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    notes += ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    notes += ["a2", "c3+", "c4+ e4 g4+", "b2"]
    duration += [n]*15 + [2*n-10] + [3*n+10, n, n, 2*n]  # Beware of interference between tracks

    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+",  "b3+", "d3+", "a2", "e3", "e3"]
    duration += [c]*13 + [n, c]
    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+",  "b3+", "d3+", "a2", "e3", "c4+", "e3"]
    duration += [c]*16

    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+",  "b3+", "d3+", "a2", "e3", "c4+"]
    duration += [c]*14 + [2*n]
    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+",  "b3+", "d3+", "a2", "e3", "c4+", "e3"]
    duration += [c]*16

    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+", "b3+", "d3+", "a2", "e3", "c4+", "e3"]
    notes += ["a2", "e3", "b3+"]
    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    duration += [c]*16 + [c, c, 2*n] + [c]*8

    notes += ["g2+", "d3+", "b3+", "d3+", "a2", "e3", "c4+", "e3"]
    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+", "b3+", "d3+", "a2", "e3", "c4+", "e3"]
    duration += 24*[c]

    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+", "b3+", "d3+", "a2", "e3", "c4+", "e3"]
    notes += ["e2", "b3", "g3+", "b3" , "c2+", "g3+", "e4", "g3+"]
    notes += ["g2+", "d3+", "b3+", "d3+", "a2", "e3", "c4+"]
    duration += 30*[c] + [3*n]

    notes +=  ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    notes +=  ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    duration += 16*[n]

    notes +=  ["e2", "g3+ b3 e4", "c2+", "g3+ c4+ e4", "g2+", "g3+ b3+ d4+", "a2", "a3 c4+ e4"]
    notes +=  ["e2"]
    duration += 8*[n] + [2*n]


    volume = lib.velocity(piano-lft, [[16, 0], [5, 1], [3, -1], [3, 1],
                                      [5, -1], [5, 1], [4, 0],
                                      [10, 0], [6, -1], [4, 0]])
    volume += lib.velocity(forte-lft, [[6, 0], [9, -1], [1, 1], [7, 0], [9, -1],
                                   [4, 1], [8, 0], [4, -1]])
    volume += lib.velocity(forte-lft, [[16, 0], [8, 0], [11, -1]])
    volume += lib.velocity(forte-lft, [[26, 0], [4, 1]])
    volume += lib.velocity(ff-lft, [[24, 0], [6, -1]])
    volume += lib.velocity(piano-lft, [[21, 0], [5, -1]])

    out = [[notes[i], duration[i], volume[i]] for i in range(len(notes))]
    trk = lib.notes2trk(out)
    mid.tracks.append(trk)
    return mid, out


def tempo(mid):
    decr = 4000
    ibs_scale = 1000
    beats = lib.rand_ev(880000, 980000, 22, scale=ibs_scale)
    beats += lib.rand_ev(1000000, 1100000, 2, scale=ibs_scale, loc=decr)
    beats += lib.rand_ev(800000, 900000, 23, scale=ibs_scale)
    beats += lib.rand_ev(1000000, 1100000, 2, scale=ibs_scale, loc=decr)
    beats += lib.rand_ev(800000, 900000, 38, scale=ibs_scale)
    beats += lib.rand_ev(800000, 900000, 2, scale=ibs_scale, seed=beats[-1], loc=decr)
    beats += lib.rand_ev(800000, 900000, 7, scale=ibs_scale, seed=beats[-1])
    beats += lib.rand_ev(800000, 1000000, 2, scale=ibs_scale, seed=beats[-1], loc=decr)
    beats += lib.rand_ev(800000, 900000, 12, scale=ibs_scale)
    beats += lib.rand_ev(800000, 1100000, 8, scale=ibs_scale, loc=decr, seed=beats[-1])
    beats += lib.rand_ev(800000, 900000, 30, scale=ibs_scale)
    beats += lib.rand_ev(800000, 1100000, 2, scale=ibs_scale, loc=decr, seed=beats[-1])
    beats += lib.rand_ev(800000, 900000, 22-1, scale=ibs_scale)
    beats += lib.rand_ev(1000000, 1100000, 8, scale=ibs_scale, loc=decr)
    trk_tempo = lib.tempo(beats)
    mid.tracks.append(trk_tempo)
    return mid, beats


def unpack_notes(notes):
    out = [0, 0, 0]
    for i in range(3):
        out[i] = [tri[i] for tri in notes]
    return tuple(out)


def WhereIs():
    """Create the Audiosegment"""
    mid = MidiFile()
    mid.ticks_per_beat = n
    mid, r1, r2 = right(mid)
    mid, l1 = left(mid)
    mid, beats = tempo(mid)
    rangec = (0, 127)
    lk1, ld1, lv1 = unpack_notes(l1)
    rk1, rd1, rv1 = unpack_notes(r1)
    rk2, rd2, rv2 = unpack_notes(r2)
    rain = lib.part2rain(beats, lk1, ld1, lv1 , rangec=rangec, left=True, uniform=False)
    rain = lib.part2rain(beats, rk2, rd2, rv2, rangec=rangec, rain=rain, left=False, uniform=False)
    rain = lib.part2rain(beats, rk1, rd1, rv1, rangec=rangec, rain=rain, left=False, uniform=False)
    return lib.mid2aud(mid) + 10, rain


if __name__ == "__main__":
    out, rain = WhereIs()
    out.export("song.wav")
    film(rain)

