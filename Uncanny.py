import numpy as np
from mido import MidiFile
from pydub.playback import play

import lib
from lib import merge
from lib import velocity as v
from film import film


n = 480
def uncanny(bt):
    keys = ['c2', 'c5', 'c2', 'c5']*bt
    keys2 = ['s', 'e4', 's', 'e4', 's', 'e4', 's', 'e4']*bt
    dur = [n]*4*bt
    dur2 = [240, 240]*4*bt
    vol = [64]*4*bt
    mid = MidiFile()
    mid.ticks_per_beat = 240
    trk = merge(keys, dur, vol)
    trk2 = merge(keys2, dur2, 2*vol)
    intrp = [[4*bt, 0, 0]]
    intrp = [[4*bt, 0, 5000]]
    mid, ibs = lib.set_ibs(mid, [200000, 400000], intrp,
                           ib_init=300000)
    print(ibs)
    mid.tracks.append(trk)
    mid.tracks.append(trk2)
    return lib.mid2aud(mid)


play(uncanny(5))

