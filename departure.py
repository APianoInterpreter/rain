import numpy as np
import numpy.random as rd
from mido import Message, MidiFile, MidiTrack
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload

import lib
from lib import notes2trk, merge, select_sf2
from lib import velocity as v
reload(lib)
from film import film

n = 480
c = int(n/2)
cc = int(c/2)
ccc = int(cc/2)
cccc = int(ccc/2)
t = int(n/3)
b = int(2*n)
nuance = np.array([40, 65])
LFT = 20
tr = 15
trb = 20
fin = 3
rpt = 2

select_sf2("Realistic Piano")

def right(vol_last, rkeys=None, rdur=None, rvol=None, trk=None, end=False):
    btac = rd.randint(10, 35)
    #P1
    notes = ["c4", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*4
    notes += ["c4", "f4", "a4-", "f4", "a4-", "f4", "a4-", "f4"]*4
    notes += ["c4", "d4", "f4", "d4", "f4", "d4", "f4", "d4"]*4
    notes += ["c4", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*4
    notes += ["b3-", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*2
    notes += ["b3", "d4", "g4", "d4", "g4", "d4", "g4", "d4"]*2
    dur = 160*[cc]
    vol = v(nuance, [[20, 2]], vel_init=vol_last)
    vol += v(nuance, [[140, 0]], vel_init=vol[-1])
    for i in range(int(len(vol)/4)):
        vol[i*4] += btac
    trk = merge(notes, dur, vol, trk=trk)

    if not rkeys:
        rkeys = notes
        rdur = dur
        rvol = vol
    else:
        rkeys += notes
        rdur += dur
        rvol += vol

    #P2
    notes = []
    for i in range(3):
        notes += ["c4", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*4
        notes += ["b3-", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*2
        notes += ["b3", "d4", "g4", "d4", "g4", "d4", "g4", "d4"]*2
    dur = 192*[cc]
    vol = v(nuance, [[192, 0]], vel_init=vol[-1])
    for i in range(int(len(vol)/4)):
        vol[i*4] += btac

    rkeys += notes
    rdur += dur
    rvol += vol

    trk = merge(notes, dur, vol, trk=trk)

    #P3
    notes = []
    for i in range(2):
        notes += ["c4", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*4
        notes += ["b3-", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*2
        notes += ["b3", "d4", "g4", "d4", "g4", "d4", "g4", "d4"]*2

    if end:
        notes += ["c4", "e4-", "g4", "e4-", "g4", "e4-", "g4", "e4-"]*6
        notes += ["c4"]
        dur = (len(notes)-1)*[cc] + [fin*n]
    else:
        dur = (len(notes))*[cc]

    vol = v(nuance, [[len(notes), 0]], vel_init=vol[-1])
    for i in range(int(len(vol)/4)):
        vol[i*4] += btac
    if end:
        vol[-1] += 20
    trk = merge(notes, dur, vol, trk=trk)

    rkeys += notes
    rdur += dur
    rvol += vol

    if end:
        return trk, rkeys, rdur, rvol

    return trk, rkeys, rdur, rvol, vol[-1]


def left(vol_last, lkeys=None, ldur=None, lvol=None, trk=None, end=False):
    #Added silence at beginning
    if isinstance(trk, MidiTrack):
        notes = ["s"]
        dur = [0]
        vol = [0]
        trk = merge(notes, dur, vol, trk=trk)
    else:
        notes = ["s"]
        dur = [0]
        vol = [0]
        trk = merge(notes, dur, vol)
    #P1
    notes = ["s", "d5"]
    dur = [(9*4+3)*n, n]
    vol = [0, nuance[1]+LFT]
    trk = merge(notes, dur, vol, trk=trk)

    if not lkeys:
        lkeys = notes
        ldur = dur
        lvol = vol
    else:
        lkeys += notes
        ldur += dur
        lvol += vol


    #P2
    notes = ["c5", "s", "e5-"]
    notes += ["d5", "s", "d5"]
    notes += ["c5", "s", "f5"]
    notes += ["e5-", "s", "f5"]
    notes += ["e5-", "c2", "a1-", "e5-"]
    notes += ["e5-", "e1-", "g1", "e5-"]
    dur = [4*n, 3*n, n]*4 + [2*n, 2*n, 3*n, n]*2
    vol = v(nuance+LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)

    lkeys += notes
    ldur += dur
    lvol += vol

    #P3
    notes = ["d5", "c2", "a1-", "c5", "f5"]
    notes += ["e5-", "e1-", "g1", "d5", "e5-"]
    notes += ["d5", "c2", "a1-", "c5", "f5"]
    notes += ["e5-", "e1-", "g1", "d5", "e5-"]
    dur = [2*n, 2*n, 2*n, n, n]*4
    if end:
        notes += ["c5", "s", "c3 c2"]
        dur += [4*n, 8*n, fin*n]

    vol = v(nuance+LFT, [[len(notes), 0]], vel_init=vol[-1])
    trk = merge(notes, dur, vol, trk=trk)

    lkeys += notes
    ldur += dur
    lvol += vol

    if end:
        return trk, lkeys, ldur, lvol
    return trk, lkeys, ldur, lvol, vol[-1]


def tempo(mid, rpt):
    ib_min = 100000
    ib_max = 350000
    ib_sc = 1000
    beats = lib.rand_ev(ib_min, ib_max, 4*11, seed=300000, loc=-4000, scale=ib_sc)
    beats += lib.rand_ev(ib_min, ib_max, 4*233, seed=beats[-1], scale=ib_sc)
    beats += lib.rand_ev(ib_min, ib_max, 4*11, seed=beats[-1], loc=4000, scale=ib_sc)
    trk = lib.tempo(beats, bt=cc)
    mid.tracks.append(trk)
    return mid, beats


def pedal(rpt):
    #Added silence
    times = [0, 4*n]

    #P1
    times += rpt*60*[0, 2*n]
    times += 6*[0, 2*n]
    return times


def song():
    """Create an Audiosegment for a given instrument"""
    rpt = 2
    mid = MidiFile()
    mid.ticks_per_beat = 120
    trk, rk, rd, rv, vol_last = right(15)
    for i in range(rpt-2):
        trk, rk, rd, rv, vol_last= right(vol_last, rk, rd, rv, trk=trk)
    trk, rk, rd, rv = right(vol_last, rk, rd, rv, trk=trk, end=True)
    mid.tracks.append(trk)
    trk, lk, ld, lv, vol_last = left(40)
    for i in range(rpt-2):
        trk, lk, ld, lv, vol_last = left(vol_last, lk, ld, lv, trk=trk)
    trk, lk, ld, lv = left(vol_last, lk, ld, lv, trk=trk, end=True)
    mid.tracks.append(trk)
    p_times = pedal(rpt)
    mid = lib.pedal_t(mid, p_times)
    mid, beats = tempo(mid, rpt)

    rangec = lib.minmax_keys(lk+rk)
    rain = lib.part2rain(beats, rk, rd, rv, rangec=rangec, tks_per_bt=120, color="jet")
    rain = lib.part2rain(beats, lk, ld, lv, rangec=rangec, tks_per_bt=120, rain=rain, color="jet")
    return lib.mid2aud(mid) + 10, rain


if __name__ == "__main__":
    out, rain = song()
    out.export("song.wav")
    #film(rain)
    play(out)

