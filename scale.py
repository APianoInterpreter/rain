import numpy as np
from mido import MidiFile
from pydub.playback import play

import lib
from lib import merge
from lib import velocity as v
from film import film


n = 480
def scale(i):
    keys = ['c4 c5', 'd4 d5', 'e4 e5', 'f4 f5', 'g4 g5', 'a4 a5','b4 b5', 'c5 c6']
    dur = [n for i in range(8)]
    vol = v([60, 80], [[8, 0]])
    mid = MidiFile()
    trk = merge(keys, dur, vol)

    mid, ibs = lib.set_ibs(mid, [900000, 1100000], [[8, 0, 100000]], ib_init=1000000)

    mid.tracks.append(trk)
    return lib.mid2aud(mid, name="scale%s.wav"%i)



for i in range(5):
    scale(i)


