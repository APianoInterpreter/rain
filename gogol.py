from mido import MidiFile
import lib
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
import os
from shutil import copyfile
from film import film
reload(lib)

instrument = "Realistic Piano"
add = 25
lft = 35
fin = 2

b = 480
c = int(b/2)
cc = int(c/2)
ccc = int(cc/2)
cccc = int(ccc/2)
t = int(b/3)
w = int(2*b)
ppp = np.array([7, 22]) + add
pp = np.array([23, 38]) + add
p = np.array([39, 54]) + add
mp = np.array([55, 65]) + add
mf = np.array([65, 75]) + add
f = np.array([87, 102]) + add

def r_1(first=True):
    """Encode the first motif"""
    notes = [[i, cccc] for i in ['g5', 'f5', 'e5', 'd5']]
    notes += [['c5+', c-4*cccc], ['d5', b+c+3*w]]
    notes += [['e5', c], ['f5', b+c], ['g5+', c], ['a5', b+c]]
    notes += [[i, c] for i in ['d6', 'e6-', 'g5', 'a5', 'b5-', 'd6', 'c5+', 'd5']]
    notes += [['e5', c], ['f5', c+w+b]]
    vel_on = lib.velocity(p, [[6, 0]])
    vel_on += lib.velocity(mp, [[14, 0]])
    if not first:
        notes[5] = ['d5', b+c+w]
        vel_on = lib.velocity(p, [[6, 0]])
        vel_on += lib.velocity(mp, [[4, 0], [8, 1], [2, -1]])
    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]
    return out, vel_on



def r_2(vel_init):
    notes = [[i, cccc] for i in ['g5', 'f5', 'e5', 'd5']]
    notes += [['c5+', c-4*cccc], ['d5', b+c+w]]
    notes += [['a6 c7+', c], ['b6- d7', b+c], ['a6 c7+', c], ['b6- d7', cc],
              ['a6 c7+', cc+c], ['b6- d7', c]]
    notes += [['d5 f5+', c], ['e5 g5', c+2*b+c], ['f5', cc], ['g5', cc]]
    notes += [['g5+', cccc], ['a5', c-cccc], ['c6', c], ['c5', c], ['d5', c],
              ['f5', c+w], ['b5-', c], ['b4-', c], ['c5', c], ['e5-', w]]
    notes += [['g4 a4 c5+', c], ['f4 a4 d5', c+w]]
    vel_on = lib.velocity(p, [[22, 0], [4, -1]], vel_init=vel_init)
    vel_on += lib.velocity(mp, [[2, 0]])

    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]
    return out


def r_3():
    notes = [['s', c], ['f4 a4 d5', c]]*4
    notes += [['g4+ e5', c], ['a4 f5', b+c],['c5 g5+', c], ['d5 a5', b], ['a5', c]]
    notes += [[i, t] for i in ['c6+', 'd6', 'e6-', 'g5', 'g5+', 'a5', 'b5-', 'd6', 'c5', 'c5+', 'd5', 'e5-']]
    notes += [['g4+ e5', c], ['a4 f5', c+w+b]]
    notes += [[i, cccc] for i in ['g5', 'f5', 'e5', 'd5']]
    notes += [['e4 c5+', c-4*cccc], ['f4 d5', b+c+w]]
    notes += [['g4 e5', c], ['a4 f5', b+c], ['e5 g5+', c], ['f5 a5', b+c]]
    notes += [[i, t] for i in ['c6+', 'd6', 'e6-', 'g5', 'g5+', 'a5', 'b5-', 'd6', 'c5', 'c5+', 'd5', 'e5-']]
    notes += [['g4+ e5', c], ['a4 f5', c+w+b]]
    vel_on = lib.velocity(ppp, [[4, 0], [4, 1]])
    vel_on += lib.velocity(mp, [[25, 0]])
    vel_on += lib.velocity(mf, [[18, 0]])

    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]

    for i in [1, 3, 5, 7]:
        gl = rd.randint(10, 30)
        out[i].append(gl)
    return out, vel_on


def r_4(vel_init):
    notes = [[i, cccc] for i in ['g5', 'f5', 'e5', 'd5']]
    notes += [['c5+', c-4*cccc], ['d5', b+c+w]]
    notes += [['a4 c5+', c], ['b4- d5', b+c], ['a4 c5+', c], ['b4- d5', cc],
              ['a4 c5+', cc+c], ['b4- d5', c]]
    notes += [['d4 f4+', c], ['e4 g4', c+w]]
    notes += [['f4 f5', c], ['g4 g5', c], ['a4 a5', c], ['c5 c6', c]]
    notes += [['c4 c5', c], ['d4 d5', c], ['f4 f5', c+w]]

    notes += [['b4- b5-', c], ['b3- b4-', c], ['c4 c5', c], ['e4- e5-', w]]
    notes += [['g3 a3 c4+', c], ['f3 a3 d4', 2*b]]
    notes += [['f3 a3 d4', c], ['f3 a3 d4', c], ['f3 a3 d4', c]]
    notes += [['s', c], ['f3 a3 d4', c], ['s', c], ['f3 a3 d4', c], ['s', c]]
    notes += 3*[['f3 a3 d4', c]]
    notes += [['f4+', c], ['g4', b+c], ['b4-', c], ['b4', c], ['d5', c], ['c5+', c]]

    notes += [['c5', c], ['a4', c], ['f4', c], ['c4', c], ['d4+', ccc], ['e4', b + c - ccc], ['c4 e4', c]]
    notes += [['b3 d4+ f4+', c], ['b3 e4 g4', b], ['b3 e4 g4', c], ['c4+ b4-', c], ['d4 b4', c], ['e4 c5+', c], ['f4 d5', c]]
    notes += [['f4+ d5+', c], ['g4 e5', c], ['b4 g5', c], ['a4 f5+', c], ['a4- f5', c], ['f4 d5', c], ['d4 b4-', c]]
    notes += [['c4+ e4 a4', c], ['g4 a4 c5+', c], ['d4 f4 a4 d5', c+w+b+b*fin]]

    vel_on = lib.velocity(mf, [[6, -1]], vel_init=vel_init)
    vel_on += lib.velocity(mp, [[19, 0], [5, 1], [5, -1]])
    vel_on += lib.velocity(p, [[13, 0], [15, 1]])
    vel_on += lib.velocity(mf, [[5, 0]])
    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]

    return out


def right(mid):
    """Assemble all the part from the right hand and add it to the midfile"""
    out, vel = r_1()
    out_t, vel = r_1(False)
    out += out_t
    out += r_2(vel[-1])
    out_t, vel = r_3()
    out += out_t
    out += r_4(vel[-1])
    trk = lib.notes2trk(out)
    mid.tracks.append(trk)
    return mid, out


def l_1(first=True):
    notes = 12 * [['d3', c], ['f3 a3 d4', c]]
    notes += 2 * [['c3', c], ["e3- g3 c4", c]]
    notes += 2 * [['g2', c], ["b2- d3 g3", c]]
    notes += 4 * [['d3', c], ['f3 a3 d4', c]]
    vel_on = lib.velocity(p-lft, [[16, 0]])
    vel_on += lib.velocity(mp-lft, [[24, 0]])

    if not first:
        notes = 8 * [['d3', c], ['f3 a3 d4', c]]
        notes += 2 * [['c3', c], ["e3- g3 c4", c]]
        notes += 2 * [['g2', c], ["b2- d3 g3", c]]
        notes += 4 * [['d3', c], ['f3 a3 d4', c]]
        vel_on = lib.velocity(p-lft, [[8, 0]])
        vel_on += lib.velocity(mp-lft, [[8, 0], [8, 1], [8, -1]])
    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]
    return out, vel_on


def l_2(vel_init):
    notes = 4 * [['d3', c], ['f3 a3 d4', c]]
    notes += 4 * [['g3', c], ['b3- d4 g4', c]]
    notes += 4 * [['c3', c], ['g3 b3- e4', c]]
    notes += 2 * [['f3', c], ['e4- a4 c5', c]]
    notes += [['b3-', c], ['a4+ d5', c]]
    notes += [['s', c], ['a4+ d5', c]]
    notes += [['b3-', b+c], ['f4+', c], ['e4- g4', w]]
    notes += [['a2 a3', c], ['d3 f3 a3', 2*b], ['f3 a3 d4', c]]
    notes += [['d3', c], ['f3 a3 d4', c]]
    vel_on = lib.velocity(p-lft, [[33, 0]], vel_init=vel_init)
    vel_on += lib.velocity(pp-lft, [[5, 0]])
    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]

    return out, vel_on


def l_3(vel_init):
    notes = 8 * [['d3', c], ['f3 a3 d4', c]]
    notes += 2 * [['c3', c], ['e3- g3 c4', c]]
    notes += 2 * [['g2', c], ['b2- d3 g3', c]]
    notes += 12 * [['d3', c], ['f3 a3 d4', c]]
    notes += 2 * [['c3', c], ['e3- g3 c4', c]]
    notes += 2 * [['g2', c], ['b2- d3 g3', c]]
    notes += 4 * [['d3', c], ['f3 a3 d4', c]]

    vel_on = lib.velocity(pp-lft, [[6, 0], [2, 1]], vel_init=vel_init)
    vel_on += lib.velocity(mp-lft, [[32, 0]])
    vel_on += lib.velocity(mf-lft, [[24, 0]])
    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]

    return out, vel_on


def l_4(vel_init):
    notes = [['c3+ e3 g3', c], ['d2 f2 a2 d3', 2*c]]
    notes += 4 * [['d3 f3 a3', c]]
    notes += [['f1+', c], ['g1', c], ['b2- d3 g3', c]]
    notes += 3 * [['g2', c], ['b2- d3 g3', c]]
    notes += 4 * [['c3', c], ['g3 b3- c4 e4', c]]
    notes += 2 * [['f2', c], ['a2 e3-', c]]
    notes += [['b2-', c], ['d3 f3 a3-', b], ['d3 f3 a3-', c]]

    notes += [['b2-', b+c], ['d3 f3+', c], ['e3- g3', w]]
    notes += [['a2 a3', c], ['d3 g3 a3', c+w+b]]
    notes += [['d2 d3', b], ['a1 a2', b], ['d1 d2', w]]
    notes += 2 * [['e2', c], ['g3', c]]
    notes += 2 * [['g2', c], ['d3 f3 g3', c]]

    notes += 2 * [['f2', c], ['c3 e3- f3', c]]
    notes += [['a2', c], ['c3 e3 a3', c]]
    notes += [['a2', c], ['e3', c]]
    notes += [['d3+', c], ['e3', b], ['g3', c], ['g2', c], ['d3 f3 g3 b3', b], ['d3 f3 g3 b3', c]]
    notes += [['c3', c], ['g3 b3- c4 e4', b], ['g3 b3- c4 e4', c]]
    notes += [['b3-', c], ['a3-', c]]
    notes += [['f3', c], ['a2 e3 g3', c]]
    notes += [['a2 e3 g3', c], ['d1 a1 d2', c+w+b+fin*b]]

    vel_on = lib.velocity(mf-lft, [[7, -1]], vel_init=vel_init)
    vel_on += lib.velocity(mp-lft, [[26, 0], [2, 1], [3, -1]])
    vel_on += lib.velocity(p-lft, [[16, 0], [13, 1]])
    vel_on += lib.velocity(mf-lft, [[2, 0]])
    out = [notes[i] + [c_vel] for i, c_vel in enumerate(vel_on)]
    return out


def left(mid):
    """Assemble all the part from the left hand and add it to the midfile"""
    out, vel = l_1()
    out_t, vel = l_1(False)
    out += out_t
    out_t, vel = l_2(vel[-1])
    out += out_t
    out_t, vel = l_3(vel[-1])
    out += out_t
    out += l_4(vel[-1])
    trk = lib.notes2trk(out)
    mid.tracks.append(trk)
    return mid, out


def tempo(mid):
    TP_VAR = 10000
    TP_MIN = 1000000
    TP_MAX = 800000
    ATEMPO = int((TP_MIN + TP_MAX)/2)
    beats = lib.rand_ev(800000, 1000000, 36, seed=ATEMPO, scale=TP_VAR)
    beats += lib.rand_ev(900000, 1200000, 4, seed=beats[-1], scale=TP_VAR, loc=50000)
    beats += lib.rand_ev(800000, 1000000, 20,  seed=ATEMPO, scale=TP_VAR)
    beats += lib.rand_ev(800000, 1000000, 8,  seed=beats[-1], scale=TP_VAR)
    beats += lib.rand_ev(700000, 900000, 4,  seed=beats[-1], scale=TP_VAR, loc=50000)
    beats += lib.rand_ev(800000, 1000000, 60,  seed=ATEMPO, scale=TP_VAR)
    beats += lib.rand_ev(900000, 1200000, 8,  seed=beats[-1], scale=TP_VAR, loc=90000)
    trk_tempo = lib.tempo(beats)
    mid.tracks.append(trk_tempo)
    return mid, beats


def Gogol():
    """Create the Audiosegment"""
    mid = MidiFile()
    mid.ticks_per_beat = b
    mid, lpart = left(mid)
    mid, rpart = right(mid)
    mid, ibs = tempo(mid)
    rain = lib.part2rain(ibs, lpart, distrib="left")
    rain = lib.part2rain(ibs, rpart, rain=rain, distrib="right")
    out = lib.mid2aud(mid) + 15
    return out, rain


if __name__ == "__main__":
    out, rain = Gogol()
    out.export("song.wav")
    film(rain)
