from mido import MetaMessage, Message, MidiFile, MidiTrack
import numpy.random as rd
import numpy as np
import midi2audio as m2a
from pydub.playback import play
from pydub import AudioSegment
import os
from shutil import copyfile
from pathlib import Path
from film import film
import matplotlib

def rand_ev(low, high, nstp, seed=None, loc=0, scale=1):
    """Generate a sequence following a random evolution using gaussian noise

    Parameters
    ----------
    low: int, minimal value of the sequence
    high: int, maximal value
    nstp: int, number of elements in the sequence
    seed: int, initial value, if none specified choose a random int between
    low and high
    loc: float, the mean value of the gaussian noise
    scale: float, the standard deviation

    Output
    ------
    rands: ints list, the entire sequence between low and high of size nstp

    Comments
    --------
    This function is the heart of quantic music
    """
    if not seed:
        seed = rd.randint(low, high)  # Generate an integer seed using a uniform law

    rands = []
    for i in range(nstp):
        seed += int(rd.normal(loc, scale))  # Generate the next value using a normal law

        # The two bounds are sticky seed cannot go beyond them.
        if seed < low:
            rands += [low]
            seed = low
        elif seed > high:
            rands += [high]
            seed = high
        else:
            rands += [seed]  # Add the int to the list

    return rands



def set_ibs(mid, ib_nuance, ib_ns, trk=None, ib_init=None):
    """"Set the interbeat intervals (ibs) of a song using rand_v to provide a unique texture to it

    Parameters
    ----------
    ib_nuance: couple of floats, minimum and maximum interbeat interval
    ib_ns: list of int triples, (number of beats, mean value of the noise, its standard deviation)
    ib_init: float, an initial ib
    tks_per_beat: int, number of ticks per beat
    """
    tks_per_bt = mid.ticks_per_beat
    ib_min = ib_nuance[0]
    ib_max = ib_nuance[1]
    if not ib_init:
        ibs = [rd.randint(ib_min, ib_max)]
    else:
        ibs = [ib_init]

    for c_ib in ib_ns:
        ibs += rand_ev(ib_min, ib_max, c_ib[0], ibs[-1], c_ib[1], c_ib[2])

    if not trk:  # Create a track to have all the temporal variation
        trk = MidiTrack()
        trk.name = "Tempo variation"

    # Set a metamessage to change the tempo between each beat
    trk.append(MetaMessage("set_tempo",
                           tempo=ibs[0],
                           time=0))

    for i, c_ib in enumerate(ibs[1:]):
        trk.append(MetaMessage("set_tempo",
                               time=tks_per_bt,
                               tempo=c_ib))

    mid.tracks.append(trk)

    return mid, ibs



def velocity(nuance, notes, vel_init=None):
    """Generate a drifting volume within a given range"""
    if not vel_init:
        scale = np.sqrt(nuance[1] - nuance[0])/2
        vel_init = int(rd.normal(loc=np.mean(nuance), scale=scale))

    vel = []
    for i, note in enumerate(notes):
        if i == 0:
            sd = vel_init
        else:
            sd = vel[-1]
        vel += rand_ev(nuance[0], nuance[1], note[0], seed=sd, loc=note[1])

    return vel


def char2note(char):
    """Convert a char to a midi note number wih the followung syntax. First
    the note ('a', 'b' , etc..) followed by the keyboard octave number and
    finally its accentuation ('-', '+'). By default the on velocity and off
    velocity are set"""
    if char == 's':  # Return a silence
        return 0
        return 0, 0, 0
    notes = {'a': 69, 'b': 71, 'c': 60, 'd': 62, 'e': 64, 'f': 65, 'g': 67}
    note_nb = notes[char[0]]
    note_nb += (int(char[1])-4) * 12
    if len(char) == 3:
        if char[2] == '-':
            note_nb -= 1
        if char[2] == '+':
            note_nb += 1
    return note_nb


def char2note(char):
    """Convert a char to a midi note number wih the followung syntax. First the note ('a', 'b' , etc..) followed by the keyboard octave number and  finally its accentuation ('-', '+'). By default the on velocity and offv elocity are set"""
    if char == 's':  # Return a silence
        return 0, 0, 0
    notes = {'a': 69, 'b': 71, 'c': 60, 'd': 62, 'e': 64, 'f': 65, 'g': 67}
    note_nb = notes[char[0]]
    note_nb += (int(char[1])-4) * 12
    if len(char) == 3:
        if char[2] == '-':
            note_nb -= 1
        if char[2] == '+':
            note_nb += 1
    v_on = 64
    v_off = 100
    return note_nb, v_on, v_off


def notes2trk(notes, trk=None):
    """Create a track from notes with a particular syntax."""
    if not trk:
        trk = MidiTrack()

    for i, note in enumerate(notes):
        if note[0] == 's':  # Message for a silence
            trk.append(Message("note_on", note=0, velocity=0, time=0))
            trk.append(Message("note_off", note=0, velocity=0, time=note[1]))
            continue

        nlist = note[0].split(' ')

        for i, c_n in enumerate(nlist):  # Add all notes as on type message
            n_nb = char2note(c_n)
            trk.append(Message("note_on", note=n_nb, velocity=note[2],
                               time=0))
        trk.append(Message("note_off", note=n_nb, velocity=100,
                           time=note[1]))

        for c_n in nlist[:-1]:  # Add the note off message
            n_nb = char2note(c_n)
            trk.append(Message("note_off", note=n_nb, velocity=100,
                               time=0))

    return trk


def notes2trk(notes, trk=None, char=True):
    """Create a track from notes with a particular syntax."""
    if not trk:
        trk = MidiTrack()

    for i, note in enumerate(notes):
        if note[0] == 's':  # Message for a silence
            trk.append(Message("note_on", note=0, velocity=0, time=0))
            trk.append(Message("note_off", note=0, velocity=0, time=note[1]))
            continue

        if char:
            nlist = note[0].split(' ')
        else:
            nlist = note[0]

        for i, c_n in enumerate(nlist):  # Add the note on message
            if char:
                n_nb, v_on, v_off = char2note(c_n)
            else:
                n_nb = c_n
                v_on = note[2]
                v_off = 100

            if len(note) == 4 and i != 0: #Implemente a glissando
                gl = note[3]
            else:
                gl = 0

            if len(note)==2:
                trk.append(Message("note_on", note=n_nb, velocity=v_on, time=gl))
            else:
                trk.append(Message("note_on", note=n_nb, velocity=note[2],
                                   time=gl))

        trk.append(Message("note_off", note=n_nb, velocity=v_off,
                           time=note[1]-gl*(len(nlist)-1)))
        for c_n in nlist[:-1]:  # Add the note off message
            if char:
                n_nb, v_on, v_off = char2note(c_n)
            else:
                n_nb = c_n
                v_on = note[2]
                v_off = 100
            trk.append(Message("note_off", note=n_nb, velocity=v_off,
                               time=0))

    return trk


def merge(notes, duration, volume, trk=None):
    """"Merge the different element to add a track to a midi file"""
    if (len(notes) != len(duration)) or (len(notes) != len(volume)):
        print(len(notes), len(duration), len(volume))
        # import pdb; pdb.set_trace()  # XXX BREAKPOINT

    l_size = max(len(notes), len(duration), len(volume))
    out = [[notes[i], duration[i], volume[i]] for i in range(l_size)]
    trk = notes2trk(out, trk)
    return trk


def merge(notes, duration, volume, gl=None, trk=None, char=True):
    """"Merge the different element to add a track to a midi file"""
    if (len(notes) != len(duration)) or (len(notes) != len(volume)):
        print(len(notes), len(duration), len(volume))
        import pdb; pdb.set_trace()  # XXX BREAKPOINT

    l_size = max(len(notes), len(duration), len(volume))
    out = [[notes[i], duration[i], volume[i]] for i in range(l_size)]
    if gl:
        for g in gl:
            out[g[0]].append(g[1])
    trk = notes2trk(out, trk, char)
    return trk


def pedal_t(mid, times, rand=True):
    """Create a pedal track on and off at given times"""
    trk = MidiTrack()
    trk.name = "Pedal"
    flip_on = True
    for t in times:
        if rand:
            p_on = rd.randint(64, 127)
            p_off = rd.randint(0, 64)
        else:
            p_on = 127
            p_off = 0
        if flip_on:
            trk.append(Message("control_change", control=64, value=p_on,
                               time=t))
            flip_on = False
        else:
            trk.append(Message("control_change", control=64, value=p_off,
                               time=t))
            flip_on = True
    mid.tracks.append(trk)
    return mid


def select_sf2(name):
    """Select an sf2 used to play a piece"""
    home = str(Path.home())
    if os.path.exists(home + "/.fluidsynth/default_sound_font.sf2"):
        os.remove(home + "/.fluidsynth/default_sound_font.sf2")
    copyfile(name + ".sf2", home + "/.fluidsynth/default_sound_font.sf2")


def mid2aud(mid, name=None):
    """Create AudioSegment from a Mid file"""
    mid.save("temp.mid")
    if name:
        m2a.FluidSynth().midi_to_audio('temp.mid', name)
        seg = None
    else:
        m2a.FluidSynth().midi_to_audio('temp.mid', 'temp.wav')
        seg = AudioSegment.from_file('temp.wav')
    os.remove("temp.mid")
    if not name:
        os.remove("temp.wav")
    return seg


def ibs2t(ibs, ib_per_bt=4):
    """Produce the time vector corresponding to change in ib"""
    t = []
    tp = 0
    t.append(tp)
    for c_ib in ibs[:-1]:
        tp += c_ib
        t.append(tp/(ib_per_bt*10**3))
    return t


def ibs2tpo(ibs):
    """Turn interbeat interval into a tempo or the other ways around"""
    return (1000000/ibs)*60


def ibs2ticksi_old(ibs, ticks_per_beat=480):
    """DEPRECATED for the number of ticks Return a list of how long a tick last"""
    ticks = []
    for c_ibs in ibs:
        time_per_tick = c_ibs/ticks_per_beat
        for i in range(ticks_per_beat):
            ticks.append(time_per_tick)
    return ticks


def ibs2ticks(nticks, ibs, tks_per_bt=480):
    """Return a list of how long a tick last in microsecods"""
    ticks = [0 for i in range(nticks)]  # Build list of ticks
    ibs_size = len(ibs)
    j, k = 0, 0

    for i, c_tick in enumerate(ticks):
        ticks[i] = ibs[j]/tks_per_bt
        if j < len(ibs)-1 and k == tks_per_bt:
            j += 1
            k = 0
        k += 1
    return ticks


def minmax_keys(keys):
    """Determine the maximum and minimum value of the keys"""
    note = []
    for k in keys:
        for i in k.split(' '):
            if i != 's':
                note.append(char2note(i))
    mink = min(note)
    maxk = max(note)

    print(mink, maxk)
    return mink, maxk


def part2rain(ibs, part, rain=None, rangec=(0, 127), echo=0, tks_per_bt=480, color="viridis", fps=24, distrib="rand"):
    """Convert a sheet of music into a rain file
    # Rain format {(xpo, ypos, time): (color, big_init, duration)}"""
    keys = [p[0] for p in part]
    dur = [p[1] for p in part]
    vol = [p[2] for p in part]
    if not rain:
        rain = {}
    r_tim, r_dur, r_vol = [], [], []
    ticks = ibs2ticks(np.sum(dur), ibs, tks_per_bt)

    ticks_prog, time, key_dur = 0, 0, 0
    for i, c_keys in enumerate(keys):
        r_tim += [int(((time + key_dur)/1000000)*fps)]
        key_dur = int(np.sum(ticks[ticks_prog:ticks_prog + dur[i]]))
        r_dur += [int((key_dur/1000000)*fps)+vol[i]]
        r_vol += [vol[i]]
        time = int(np.sum(ticks[:ticks_prog]))
        ticks_prog += dur[i]

    if color != "black":
        cmap = matplotlib.cm.get_cmap(color)
        # determine the range of the keys
        mink, maxk = rangec
        rangek = maxk-mink
        rangec = np.linspace(0, 1, rangek, endpoint=True)

    for i, key in enumerate(keys):
        #rd.seed(r_tim[i]) #To have a similar rain if ibs is constant
        if key == 's':
            continue
        for j, k in enumerate(key.split(' ')):
            nb_note = char2note(k)[0]
            if color != "black":
                #rgba = cmap(char2note(k)/127)
                rgba = cmap(rangec[nb_note-mink-1])
            else:
                rgba = (0, 0, 0)

            if distrib == "left":
                xpos, ypos = (rd.uniform(-9, 0), rd.uniform(-9, 9))
            elif distrib == "right" :
                xpos, ypos = (rd.uniform(0, 9), rd.uniform(-9, 9))
            elif distrib == "deter":
                coor = np.linspace(-10, 10, 12)
                x = coor[nb_note % 12]
                y = coor[int(np.floor(nb_note/12))]
                xpos, ypos = (x, y)
            else:
                xpos, ypos = (rd.uniform(-9, 9), rd.uniform(-9, 9))

            rain[(xpos, ypos, r_tim[i])] = (rgba, r_dur[i], r_vol[i])
    return rain


def max_t_rain(rain):
    """Determine the maximum time of a rain"""
    time_max = 0
    # Determine the maximum time
    for items in rain.items():
        time_max = max(time_max, items[0][-1] + items[1][-2])
    return time_max


def background_rain(rain, max_drops=10):
    """Generate a background rain

    Improvements
    ------------
    Add ibs to follow the change in rythmi
    """
    max_t = max_t_rain(rain)
    durs = [0, 0.2, 0.4, 0.6, 0.8, 1]
    drops = [int(max_drops)/10, int(max_drops/2), max_drops, int(max_drops/2), int(max_drops)/10]
    for i, drop in enumerate(drops):
        for j in range(i):
            bdur = max_t * durs[j]
            edur = max_t * durs[j+1]
            xpos = rd.uniform(-9, 9)
            ypos = rd.uniform(-9, 9)
            dur = rd.randint(200, 480)
            vol = int(rd.normal(loc=64, scale=10))
            t = int(rd.uniform(bdur, edur))
            rain[(xpos, ypos, t)] = ((0, 0, 0), dur, vol)
    return rain


def tempo(beats, trk=None, bt=480):
    """Create a track setting a tempo at each new beat.    DEPRECATED used for retro compatibility"""
    if not trk:
        trk = MidiTrack()
        trk.name = "Tempo variation"
    trk.append(MetaMessage("set_tempo",
                           tempo=beats[0],
                           time=0))

    for i, beat in enumerate(beats[1:]):
        trk.append(MetaMessage("set_tempo",
                               time=bt,
                               tempo=beat))

    return trk


def gen_part(key, dur, vol, init=[]):
    out = init
    out += [[key[i], dur[i], vol[i]] for i in range(len(key))]
    return out


if __name__ == "__main__":
    trk = None
    select_sf2("Realistic Piano")
    mid = MidiFile()
    mid.ticks_per_beat = 240
    bt = 240
    # Add the scale to a mid file
    key = ["c3", "d3", "e4 e3", "s", "f4", "g4", "a4", "b4",
           "c5"]
    #key = ["c4", "d4", "e4", "f4", "g4", "a4", "b4", "c5"]
    # Set a constant duration of a Whole here
    dur = [4*bt, bt,  bt, int(bt/2), int(bt/4), int(bt/4), int(bt/4), int(bt/4), int(bt/2)]
    #dur = [int(bt/8) for i in range(4)] + [2*bt for i in range(4)]
    # Vary the velocity/volume for each key stroke
    vol = velocity([0, 127], [[9, 4]])
    #vol = velocity([0, 127], [[8, 4]])

    # Merge all and add to the mid
    trk = merge(key, dur, vol, gl=[[0,50]], trk=trk)
    mid.tracks.append(trk)

    # Make the four beats interval evolve from 1000000
    # mid, ibs = set_ibs(mid, [800000, 1200000], [[15, 1000, 100000]], ib_init=1000000, tks_per_bt=240)
    mid, ibs = set_ibs(mid, [800000, 1200000], [[8, 1000, 100000]], ib_init=1000000)

    # Generate the mid file
    aud = mid2aud(mid)
    aud.export("song.wav")
    fps = 24
    rain = part2rain(ibs,[[key[i], dur[i], vol[i]] for i in range(len(key))], fps=fps, tks_per_bt=mid.ticks_per_beat)

    #print(minmax_keys(key))
    #play(aud)
    rainb = background_rain(rain, aud.duration_seconds)
    film(rainb, fps=fps)
    # mid.print_tracks()
