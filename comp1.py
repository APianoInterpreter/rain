from mido import MidiFile, MidiTrack
import lib
from lib import velocity as v
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
import os
from shutil import copyfile
from lib import gen_part
reload(lib)
from film import film
if os.path.exists("../../.fluidsynth/default_sound_font.sf2"):
    os.remove("../../.fluidsynth/default_sound_font.sf2")
copyfile("Realistic Piano.sf2", "../../.fluidsynth/default_sound_font.sf2")
#copyfile("P120New.sf2", "../../.fluidsynth/default_sound_font.sf2")
lft = 15
add = 30
fin = 10

pp = np.array([0, 15]) + add
p = np.array([15, 30]) + add
mp = np.array([30, 45]) + add
mf = np.array([45, 60]) + add

b = 480
c = int(b/2)
cc = int(c/2)
t = int(b/3)
w = int(2*b)


def right_motif1(nuance, vel_init=None, second=False):
    if not second:
        notes = [['s', c], ['g4', cc], ['f4+', cc], ['g4', c], ['b4', cc], ['c5', cc], ['b4', w],
                 ['s', c], ['f4+', cc], ['g4', cc], ['f4+', c], ['g4', cc], ['a4', cc], ['g4', w],
                 ['s', c], ['f4+', cc], ['e4', cc], ['f4+', c], ['b4', cc], ['c5', cc], ['b4', w],
                 ['s', c], ['f4+', cc], ['e4', cc], ['f4+', w+b]]
    else:
        notes = [['s', c], ['g5', cc], ['f5+', cc], ['g5', c], ['b5', cc], ['c6', cc], ['b5', w],
                 ['s', c], ['f5+', cc], ['g5', cc], ['f5+', c], ['g5', cc], ['a5', cc], ['g5', w],
                 ['s', c], ['f5+', cc], ['e5', cc], ['f5+', c], ['b5', cc], ['c6', cc], ['b5', w],
                 ['s', c], ['f5+', cc], ['e5', cc], ['f5+', w+b]]

    vel = lib.velocity(nuance, [[8, 0], [3, 1], [4, -1], [3, 1], [4, -1], [3, -1]],
                       vel_init=vel_init)
    acc = [0, 7, 14, 21]
    for i in acc:
        vel[i] += 5
    rhand = [notes[i] + [c_vel] for i, c_vel in enumerate(vel)]
    return rhand, vel


def right_motif2(nuance1, nuance2, vel_init=None, second=False):
    if not second:
        notes = [['e5', b+c], ['b4', c+w], ['d5', b+c], ['b4', w+c],
                       ['f5+', b+c], ['b4', c+w], ['f5+', b+c], ['a4', w+c]]
        notes += [['b4 g5', b+c], ['g4 e5', c+w], ['b4 g5', b+c], ['g4 d5', w+c],
                        ['b4 f5+', b+c], ['f4+ d5', c+w], ['a4 f5+', b+c], ['f4+ d5', w+c]]
    else:
        notes = [['e6', b+c], ['b5', c+w], ['d6', b+c], ['b5', w+c],
                       ['f6+', b+c], ['b5', c+w], ['f6+', b+c], ['a5', w+c]]
        notes += [['b5 g6', b+c], ['g5 e6', c+w], ['b5 g6', b+c], ['g5 d6', w+c],
                        ['b5 f6+', b+c], ['f5+ d6', c+w], ['a5 f6+', b+c], ['f5+ d6', w+c]]
    vel = lib.velocity(nuance1, [[2, 0], [2, 1], [2, 0], [2, -1]], vel_init=vel_init)
    if nuance1[0] < nuance2[0]:
        vel += lib.velocity(nuance2, [[2, 0], [2, 1], [2, 0], [2, -1]], vel_init=vel[-1]+15)
    else:
        vel += lib.velocity(nuance2, [[2, 0], [2, 1], [2, 0], [2, -1]], vel_init=vel[-1]-15)

    rhand = [notes[i] + [c_vel] for i, c_vel in enumerate(vel)]
    return rhand, vel


def right_motif3(nuance, vel_init=None, rep=False, first=True, second_nuance=None):
    if first:
        notes = 4*['b4', 'e5', 'b5'] + ['b4', 'e5', 'c5', 'e5']
        notes += 4*['b4', 'd5', 'b5'] + ['b4', 'd5', 'a4', 'd5']
        notes += 4*['f4+', 'b4', 'f5+'] + ['f4+', 'b4', 'g4', 'b4']
        if rep:
            notes += 4*['a4', 'd5', 'a5'] + ['a4', 'd5', 'a5']
        else:
            notes += 4*['a4', 'd5', 'a5'] + ['a4', 'd5', 'g4', 'd5']
    else:
        notes = 4*['b5', 'e6', 'b6'] + ['b5', 'e6', 'c6', 'e6']
        notes += 4*['b5', 'd6', 'b6'] + ['b5', 'd6', 'a5', 'd6']
        notes += 4*['f5+', 'b5', 'f6+'] + ['f5+', 'b5', 'g5', 'b5']
        if not rep:
            notes += 4*['a5', 'd6', 'a6'] + ['a5', 'd6', 'g5', 'd6']
        else:
            notes += 4*['a5', 'd6', 'a6'] + ['a5', 'd6', 'a6', 'g6']

    acc = np.array([0, 12, 14])
    if rep:
        acc_t = acc.tolist() + (acc + 16).tolist() + (acc + 32).tolist() + (acc + 48).tolist()
    else:
        acc_t = acc.tolist() + (acc + 16).tolist() + (acc + 32).tolist() + [48]

    if second_nuance is not None:
        if rep:
            acc_t = acc.tolist() + (acc + 16).tolist()
        else:
            acc_t = acc.tolist() + [16]

    if first:
        if rep:
            vel = lib.velocity(nuance, [[16, 0], [16, 1], [16, 0],
                                        [15, -1]], vel_init=vel_init)
        else:
            vel = lib.velocity(nuance, [[16, 0], [16, 1], [16, 0],
                                        [16, -1]], vel_init=vel_init)
    else:
        vel = lib.velocity(nuance, [[16, 0], [16, 1]], vel_init=vel_init)
        if nuance[0] > second_nuance[0]:
            vel += lib.velocity(second_nuance, [[16, 0], [16, -1]], vel_init=vel[-1]-15)
        else:
            vel += lib.velocity(second_nuance, [[16, 0], [16, -1]], vel_init=vel[-1]+15)

    for i in acc_t:
        vel[i] += 10

    rhand = [[notes[i], cc] + [c_vel] for i, c_vel in enumerate(vel)]
    if rep and first:
        rhand[-1][1] = c
    return rhand, vel


def left_motif(nuance, trk=None, trk1=None, vel_init=None,
               second_nuance=None,
               last=False):
    """Make the motif repeat with distinct initial values and min/max"""
    notes = 4 * ['e4', 'b3']
    notes += 4 * ['d4', 'b3']
    notes += 4 * ['d4', 'b3']
    if not last:
        if second_nuance is None:
            vel = v(nuance-lft, [[8, 0], [8, 1], [8, 0], [8, -1]], vel_init=vel_init)
        else:
            vel = v(nuance-lft, [[8, 0], [8, 1]], vel_init=vel_init)
            if nuance[0] > second_nuance[0]:
                vel += v(second_nuance-lft, [[8, 0], [8, -1]], vel_init=vel[-1]-15)
            else:
                vel += v(second_nuance-lft, [[8, 0], [8, -1]], vel_init=vel[-1]+15)
        notes += 4 * ['d4', 'a3']
        dur = 32*[c]
    else:
        if second_nuance is None:
            vel = v(nuance-lft, [[8, 0], [8, 1], [8, 0], [8, -1]], vel_init=vel_init)
        else:
            vel = v(nuance-lft, [[8, 0], [8, 1]])
            if nuance[0] > second_nuance[0]:
                vel += v(second_nuance-lft, [[8, 0], [7, -1]], vel_init=vel[-1]-15)
            else:
                vel += v(second_nuance-lft, [[8, 0], [7, -1]], vel_init=vel[-1]+15)
        #vel += [rd.randint(pp[0], pp[1])]
        vel += [vel[-1]-2]
        notes += 3 * ['d4', 'a3'] + ['d4', 'e4 b3 e3']
        dur = 30*[c] + [b, 2*b]
    trk = lib.merge(notes, dur, vel, trk=trk)

    notes1 = ['e3', 'g3', 'e3', 'g3']
    notes1 += ['d3', 'g3', 'd3', 'g3']
    notes1 += ['d3', 'f3+', 'd3', 'f3+']
    notes1 += ['d3', 'f3+', 'd3', 'f3+']
    dur1 = 16*[b]
    vel1 = [vel[i] for i in range(0, len(vel), 2)]
    trk1 = lib.merge(notes1, dur1, vel1, trk=trk1)
    return trk, notes, dur, vel, trk1, notes1, dur1, vel1


def left(mid):
    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(p)
    # Keep track of all thinngs
    lhk = notes
    lhd = dur
    lhv = vel
    lhk1 = notes1
    lhd1 = dur1
    lhv1 = vel1
    for i in range(2):
        trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(mf, trk, trk1)
        # Keep track of all thinngs
        lhk += notes
        lhd += dur
        lhv += vel
        lhk1 += notes1
        lhd1 += dur1
        lhv1 += vel1

    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(p, trk, trk1)
    # Keep track of all thinngs
    lhk += notes
    lhd += dur
    lhv += vel
    lhk1 += notes1
    lhd1 += dur1
    lhv1 += vel1

    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(mf, trk, trk1)
    # Keep track of all things
    lhk += notes
    lhd += dur
    lhv += vel
    lhk1 += notes1
    lhd1 += dur1
    lhv1 += vel1
    for i in range(2):
        trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(mf, trk, trk1, vel_init=vel[-1])
        # Keep track of all things
        lhk += notes
        lhd += dur
        lhv += vel
        lhk1 += notes1
        lhd1 += dur1
        lhv1 += vel1
    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(p, trk, trk1)
    # Keep track of all things
    lhk += notes
    lhd += dur
    lhv += vel
    lhk1 += notes1
    lhd1 += dur1
    lhv1 += vel1
    for i in range(2):
        trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(p, trk, trk1, vel_init=vel[-1])
        # Keep track of all things
        lhk += notes
        lhd += dur
        lhv += vel
        lhk1 += notes1
        lhd1 += dur1
        lhv1 += vel1
    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(mf, trk, trk1)
    # Keep track of all things
    lhk += notes
    lhd += dur
    lhv += vel
    lhk1 += notes1
    lhd1 += dur1
    lhv1 += vel1
    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(p, trk=trk, trk1=trk1, vel_init=vel[-1], second_nuance=mf)
    # Keep track of all things
    lhk += notes
    lhd += dur
    lhv += vel
    lhk1 += notes1
    lhd1 += dur1
    lhv1 += vel1
    trk, notes, dur, vel, trk1, notes1, dur1, vel1 = left_motif(p, trk=trk, trk1=trk1, vel_init=vel[-1], second_nuance=mf, last=True)
    # Keep track of all things
    lhk += notes
    lhd += dur
    lhv += vel
    lhk1 += notes1
    lhd1 += dur1
    lhv1 += vel1
    mid.tracks.append(trk)
    mid.tracks.append(trk1)
    return mid, lhk, lhd, lhv, lhk1, lhd1, lhv1


def right(mid):
    rh = [['s', 8*w]]
    vol = [0]
    rh_t, vel = right_motif1(mf)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif1(mf, vel_init=vel[-1])
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif2(p, mf, vel_init=vel[-1])
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif3(mf, vel_init=vel[-1], first=True, rep=False)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif3(mf, vel_init=vel[-1], first=True, rep=True)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif1(p, vel_init=vel[-1], second=True)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif1(p, vel_init=vel[-1], second=True)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif2(p, mp, vel_init=vel[-1], second=True)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif3(p, vel_init=vel[-1], first=False, rep=False,
                             second_nuance=mf)
    rh += rh_t
    vol += vel
    rh_t, vel = right_motif3(p, vel_init=vel[-1], first=False, rep=True,
                             second_nuance=mf)
    vel_fin = rd.randint(pp[0], pp[1])
    vel +=  [vel_fin]
    rh += rh_t + [["g5 b5 e6", w, vel_fin]]
    vol += vel
    trkr = lib.notes2trk(rh)
    mid.tracks.append(trkr)
    return mid, rh, vol


def tempo(mid, ib_scale):
    beats = lib.rand_ev(600000, 700000, 16*27, scale=ib_scale)
    beats += lib.rand_ev(600000, 700000, 16, scale=ib_scale, seed=beats[-1], loc=3000)
    beats += lib.rand_ev(600000, 700000, 16*23, scale=ib_scale, seed=rd.randint(600000, 640000))
    beats += lib.rand_ev(1800000, 1900000, 16, scale=ib_scale, seed=beats[-1]+1200000, loc=8000)

    beats = lib.rand_ev(600000, 700000, 4*27, scale=ib_scale)
    beats += lib.rand_ev(600000, 700000, 4, scale=ib_scale, seed=beats[-1], loc=3000)
    beats += lib.rand_ev(600000, 700000, 4*23, scale=ib_scale, seed=rd.randint(600000, 640000))
    beats += lib.rand_ev(1800000, 1900000, 4+2, scale=ib_scale, seed=beats[-1]+1200000, loc=8000)

    trk_tempo = lib.tempo(beats, bt=480)
    mid.tracks.append(trk_tempo)
    return mid, beats


def song():
    """Create the Audiosegment"""
    ib_scale=2500
    mid = MidiFile()
    mid.ticks_per_beat = 480
    mid, lhk, lhd, lhv, lhk1, lhd1, lhv1 = left(mid)
    mid, rh, vols = right(mid)
    keys = [c[0] for c in rh]
    durs = [c[1] for c in rh]
    mid, beats = tempo(mid, 1000)
    out = lib.mid2aud(mid) + 10
    partl = gen_part(lhk, lhd, lhv, init=[])
    partl1 = gen_part(lhk1, lhd1, lhv1, init=[])
    partr = gen_part(keys, durs, vols)
    rain = lib.part2rain(beats, partl , tks_per_bt=480, distrib='left')
    rain = lib.part2rain(beats, partl1 , tks_per_bt=480, rain=rain, distrib='left')
    rain = lib.part2rain(beats, partr , tks_per_bt=480, rain=rain, distrib='right')
    return out, rain



if __name__=="__main__":
    out, rain = song()
    out.export("song.wav")
    film(rain)
    play(out)

