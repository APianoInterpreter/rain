import numpy as np
from mido import MidiFile
from pydub.playback import play
from film import film

import lib

n = 480
c = int(n/2)
tc = int(c/3)
cc = int(c/2)
ccc = int(cc/2)
cccc = int(ccc/2)
t = int(n/3)
b = int(2*n)
piano = np.array([45, 60])
LFT = 15
tr = 15
trb = 20
fin = 8
acflt = 0

def right(mid):
    notes = ["e5", "d5+", "e5", "d5+", "e5", "b4", "d5", "c5", "a4"]
    dur = 9*[cc]
    vol = lib.velocity(piano, [[len(notes), 0]])

    trk = lib.merge(notes, dur, vol)
    # track notes, vols and durs
    keys = notes
    vols = vol
    durs = dur

    mid.tracks.append(trk)
    return mid, keys, durs, vols


def ib(mid):
    ib_var = 1000
    ib_nuance = [200000, 300000]
    mid, ibs = lib.set_ibs(mid, ib_nuance,[[8, 0, ib_var]], tks_per_bt=120)
    return mid, ibs


def song():
    """Create an Audiosegment for a given instrument"""
    lib.select_sf2("Realistic Piano")
    mid = MidiFile()
    mid, k, d, v = right(mid)
    mid, ibs = ib(mid)
    rain ={}
    rain = lib.part2rain(ibs, k, d, v, tks_per_bt=120)

    return lib.mid2aud(mid)+15, rain


if __name__ == "__main__":
    out, rain = song()
    out.export("song.wav")
    film(rain)
    play(out)
