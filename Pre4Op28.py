from mido import Message, MidiFile, MidiTrack
import lib
from lib import notes2trk, merge, select_sf2
from lib import velocity as v
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
from film import film
reload(lib)

n = 480
c = int(n/2)
cc = int(c/2)
ccc = int(cc/2)
cccc = int(ccc/2)
t = int(n/3)
w = int(2*n)
forte = np.array([60, 80])
piano = np.array([40, 60])
ppiano = np.array([30, 40])
lft = 20
fin = 1

def right(mid):
    #L1-3
    notes = ["b3", "b4"] + 3*["b4", "c5"]
    notes += ["b4", "b4-", "a4", "b4", "a4", "b4", "a4", "b4", "a4"]
    notes += ["a4", "g4+", "a4", "b4", "d5", "c5", "g4", "a4",
              "f4+", "a4", "f4+", "b4", "a4", "g4", "f4+", "c4",
              "b3", "d4", "f4+", "d5", "c5", "b4"]
    dur = [c+cc, cc] + 3*[3*n, n]
    dur += 3*[3*n, n] + [3*n, c+cc, cc]
    dur += [3*n, 2*n] + 6*[c] + [3*n, n] + [3*n, ccc, n-ccc] + 6*[c] + 3*[t]
    vol = v(piano, [[19, 0], [2, 1], [4, -1], [7, 0], [4, 1], [3, -1]])
    vol[18] += 10
    vol[32] += 10
    trk = merge(notes, dur, vol)
    ds = dur
    vs = vol
    ks = notes

    #L4-6
    notes = 3*["b4", "c5"] + ["b4", "a4+", "a4+", "g5", "f5+", "e5"]

    notes += ["e5", "d5+", "c6", "d5+", "d5+", "e5", "g5", "b4"]
    notes += ["d5", "c5", "e5", "e4", "a4",
              "f4+", "a4", "f4+", "b4", "a4", "f4+", "e4"]

    notes += ["e4", "f4+", "e4", "f4+", "e4", "s", "e3 f3+ b3 e4", "d3+ f3+ b3 d4+", "e3 g3 b3 e4"]
    dur = 3*[3*n, n] + [c+cc, cc, n, n, c+cc, cc]
    dur += 10*[c] + 3*[t] + [n+c, c, 3*n, ccc, n-ccc, 3*n+c+cc, cc]
    dur += 2*[3*n, n] + [2*n, fin*n] + 2*[2*n] + [4*n]
    vol = v(piano, [[6, 0], [3, 1], [3, -1]], vel_init = vol[-1])
    vol += v(forte, [[4, 3], [11, -1]])
    vol += v(piano, [[4, 0], [1, 1], [6, -1]])
    vol += v(ppiano, [[3, 0]])
    trk = merge(notes, dur, vol, trk=trk)
    mid.tracks.append(trk)
    ds += dur
    vs += vol
    ks += notes
    return mid, ks, ds, vs


def left(mid):
    #L1-3
    notes = ["s"] + 8*["g3 b3 e4"] + 4*["f3+ b3 e4"] + 4*["f3+ b3 e4-"] + 4*["f3 a3 e4-"] + 2*["f3 b3 d4"] + 2*["f3 g3+ d4"]
    notes += 4*["e3 g3+ d4"] + 2*["e3 g3 d4"] + 2*["e3 g3 c4+"] + 4*["e3 g3 c4"] + 8*["e3 f3+ c4"] + 4*["d3+ f3+ c4"] + 8*["d3 f3+ c4"]
    notes += 4*["d3 f3 c4"] + 4*["d3 f3 b3"] + 2*["c3 e3 b3"] + 6*["c3 e3 a3"] + 2*["b2 e3 a3"] + 2*["b2 d3+ a3"] + 4*["c3 e3 a3"]
    notes += 4*["b2 d3+ a3"] + 4*["c3 e3 a3"] + ["b2 d3+ a3", "s"]
    dur = [n] + 88*[c] + [n, 3*n]
    vol = v(piano-lft, [[66, 0], [3, 1], [4, -1], [18, 0]])
    trk = merge(notes, dur, vol)
    lks = notes
    lvs = vol
    lds = dur

    #L3-6
    notes = 8*["g3 b3 e4"] + 4*["f3+ a3 e4"] + 4*["f3+ a3 e4-"] + 2*["f3 a3- e4-"] + 2*["f3 a3- d4"] + 4*["e3 g3+ d4"]
    notes += 2*["e3 g3 d4"] + 2*["e3 g3 c4+"] + 2*["c3+ e3 a3+"] + 2*["c3 e3 a3"]

    notes += ["b2 b1"] + 3*["a3 c4 f4+ a4"] + ["g3 b3 d4+ f4+"] + 3*["g3 b3 e4"] + 2*["a3 c4 e4"] + ["a2", "e3 f3+ c4"]
    notes += 2*["b2 e3 b3"] + 2*["c3 e3 a3"] + 4*["b2 e3 b3"] + 4*["c3 e3 a3"] + 4*["b2 e3 b3"] + 2*["b2 d3+ b3"] + 2*["b2 d3+ a3"]

    notes += 4*["c3 g3"] + 2*["c3 b3-"] + 2*["c3 e3 a3"] + 2*["b2 e3 a3"] + 2*["b2 e3 g3+"]
    notes += 4*["b2 e3 g3"] + ["b2- c3 g3", "s", "b1 b2", "b1 f1+ b2", "e1 e0"]

    dur = 80*[c] + [2*n, fin*n] + 2*[2*n] + [4*n]

    vol = v(piano-lft, [[24, 0], [4, 1], [4, -1]], vel_init = vol[-1])
    vol += v(forte-lft, [[4, 1], [12, -1]])
    vol += v(piano-lft, [[14, 0], [2, 1], [18, -0.5]])
    vol += v(ppiano-lft, [[3, 0]])

    lks += notes
    lds += dur
    lvs += vol

    trk = merge(notes, dur, vol, trk=trk)
    mid.tracks.append(trk)
    return mid, lks, lds, lvs


def pedal():
    times = [n, 4*n] + [0, 4*n]*12
    times += 4*[0, 4*n] + [0, 2*n] + [3*n, n]
    return times


def Pre4():
    """Create the Audiosegment"""
    mid = MidiFile()
    mid.ticks_per_beat = n
    mid, keys, durs, vols = right(mid)
    mid, lkeys, ldurs, lvols = left(mid)

    p_times = pedal()
    mid = lib.pedal_t(mid, p_times)
    TP_VAR = 20000
    #n_beat = [2 + 4*15, 4, 4, 4, 7*4]
    #loc = [0, -1000, 0, 1000, 500]
    #mid, ibs = lib.set_ibs(mid, (1200000, 1400000), [(n_beat[i], loc[i], TP_VAR) for i in range(5)])
    #More Elaborated intepretation
    mes = [[2, 2] for i in range(15)]
    mesure = [item for sublist in mes for item in sublist]
    mes2 = [[-50000, 50000] for i in range(15)]
    mesure2 = [item for sublist in mes2 for item in sublist]
    n_beat = [2] + mesure + [4, 3, 1, 4] + [4 for i in range(7)]
    loc = [0] + mesure2 + [50000, -50000, 150000, -50000] + [7500 for i in range(7)]
    mid, ibs = lib.set_ibs(mid, (1000000, 1400000), [(n_beat[i], loc[i], TP_VAR) for i in range(len(n_beat))], ib_init=1300000)
    out = lib.mid2aud(mid)
    rain = lib.part2rain(ibs, lkeys, ldurs, lvols, color="jet")
    rain = lib.part2rain(ibs, keys, durs, vols, rain=rain, color="jet")
    return out+15, rain


out, rain = Pre4()
out.export("song.wav")
max_t = lib.max_t_rain(rain)
#rain = lib.background_rain(rain, 1000)
film(rain)
#play(out)
