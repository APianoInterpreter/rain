from mido import MidiFile
import lib
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
import os
from shutil import copyfile
from film import film
reload(lib)

instrument = "Realistic Piano"

def gen_rain(ndrops, lag=100, dur=20, octa="4"):
    """Generate a partition in CM"""
    #scale = ["c", "d", "e", "f", "g", "a", "b"]
    scale = ["c", "e", "g"]
    scale = [i + octa for i in scale] + ["s"]
    keys = ["s"]
    durs = [lag]
    vol = [0]
    keys += rd.choice(np.array(scale),ndrops).tolist()
    bound = rd.normal(loc=dur, scale=20, size=ndrops)
    bound[bound<1] = 1
    durs += [int(i) for i in bound]
    bound = rd.normal(loc=20, scale=5, size=ndrops)
    bound[bound>127] = 127
    bound[bound<0]=0
    vol += [int(i) for i in bound]
    return [[keys[i], durs[i], vol[i]] for i in range(ndrops+1)], np.sum(durs)



rain=False
if __name__ == "__main__":
    mid = MidiFile()
    part, dur = gen_rain(200, 3*480, dur=400, octa="4")
    part2, dur = gen_rain(100, 6*480, dur=400, octa="3")
    part3, dur = gen_rain(50, 9*480, dur=400, octa="5")
    trk = lib.notes2trk(part)
    trk2 = lib.notes2trk(part2)
    trk3 = lib.notes2trk(part3)
    mid.tracks.append(trk)
    mid.tracks.append(trk2)
    mid.tracks.append(trk3)
    out = lib.mid2aud(mid) + 15
    if not rain:
        play(out)
    else:
        ibs = [500000 for i in range(int(dur/480))]
        rain = lib.part2rain(ibs, part)
        rain = lib.part2rain(ibs, part2, rain=rain)
        #out.export("song.wav")
        #film(rain)

