from mido import MidiFile
import lib
import numpy as np
import numpy.random as rd
from pydub import AudioSegment
from pydub.playback import play
from importlib import reload
import os
from shutil import copyfile
reload(lib)

if os.path.exists("../../.fluidsynth/default_sound_font.sf2"):
    os.remove("../../.fluidsynth/default_sound_font.sf2")
copyfile("Realistic Piano.sf2", "../../.fluidsynth/default_sound_font.sf2")

bt = 480
cl = int(bt/2)
c = int(bt/4)
bp = cl + int(cl/2)
bl = int(2*bt)
piano = np.array([80, 100])
lft = 20

def right(mid, rand):
    notes = [[64], [61+12], [62+12], [62, 67+1, 71],
             [62, 67+1, 71],[62, 67+1, 71]]
    dur = [bt, bp, c, bt, bt, bl]
    notes += [[62+12, 66+12], [60+12, 63+12], [61+12, 64+12],
              [61+12, 69+12], [61+12, 69+12], [61+12, 69+12]]
    dur += [bt, bp, c, bt, bt, 2*bt]
    notes += [[64, 61+12], [62, 70], [71], [62+12], [62+12], [62+12]]
    dur += [bt, bp, c, bt, bt, bl]
    notes += [[62, 68], [62, 68], [61, 69], [61, 61 + 12],
              [61, 61+12], [61, 61+12], [64]]
    dur += [bt, bp, c, bt, bt, bl, 2*bt]

    notes += [[62, 68, 71], [62, 68, 71], [62, 68, 71]]
    dur += [bt, bt, bl]
    notes += [[62+12, 66+12], [72, 63+12], [61+12, 64+12],
              [61+12, 64+12, 69+12, 61+24],
              [61+12, 64+12, 69+12, 61+24],
              [70, 61+12, 64+12, 70+12, 61+24]]
    dur += [bt, bp, c, bt, bt, bl]
    notes += [[70, 61+12], [70, 61+12], [71, 62+12],
              [71, 66+12],
              [70, 66+12],
              [68, 66+12]]
    dur += [bt, bp, c, bt, bt, bl]
    notes += [[62, 68], [62, 71], [61, 69]]
    dur += [bt, bp, c]
    notes += [[61+12, 69+12], [61+12, 69+12], [61+12, 69+12]]  # Couple 69
    dur += [bt, bt, bl]
    if rand:
        vol = lib.velocity(piano, [[29, 0], [4, 1], [13,0]], vel_init=80)
    else:
        vol = [80 for i in range(29)] + [82, 84, 86, 88] + [88 for i in range(13)]
    trk = lib.merge(notes, dur, vol, char=False)

    notes = ["s", "d4"]
    dur1 = [13*bt + bp, c+4*bt-5]
    notes += ["s", "c5+", "d5"]
    dur1 += [7*bt+5, bp, c]
    vol1 = 2*[vol[15]] + 3*[vol[24]]
    trk1 = lib.merge(notes, dur1, vol1)

    notes = ["s", "g4+"]
    dur2 = [25*bt, bt]
    vol2 = 2*[vol[24]]
    trk2 = lib.merge(notes, dur2, vol2)

    mid.tracks.append(trk)
    mid.tracks.append(trk1)
    mid.tracks.append(trk2)
    return mid



def left(mid, rand):
    notes = [[0]]
    dur = [bt]
    notes += [[64-24], [64-12, 64],  [64-12, 64], [64-12, 64], [0]]
    notes += [[69-24], [69-12, 64],  [69-12, 64], [69-12, 64], [0]]
    notes += [[64-24], [64-12, 71-12],  [64-12, 71-12], [64-12, 71-12], [0]]
    notes += [[64-24], [64-12, 64],  [64-12, 64], [64-12, 64], [0]]
    notes += [[69-36], [64-12, 69-12, 64], [64-12, 69-12, 64], [69-12, 69-12, 64], [0]]
    notes += [[69-24], [69-12, 64, 69], [69-12, 64, 69], [66-12, 64, 61, 66], [0]]
    notes += [[71-36], [66-12, 62], [66-12, 71-12, 62], [66-12, 71-12, 62], [0]]
    notes += [[69-36], [64-12, 69-12, 64], [64-12, 69-12, 64], [69-12, 64], [0]]
    dur += [bt, bt, bt, bl, bt]*8
    if rand:
        vol = lib.velocity(piano-lft, [[26,0], [3, 1], [12, 0]], vel_init=80-lft)
    else:
        vol = [80-lft for i in range(26)] + [(80-lft) + 2, (80-lft) +4, (80-lft) + 6] + [(80-lft)+6 for i in range(12)]
    trk = lib.merge(notes, dur, vol, char=False)
    mid.tracks.append(trk)
    return mid


def tempo(mid, rand=False):
    if rand:
        beats = lib.rand_ev(800000, 1200000, 49, 10000, seed=1000000)
    else:
        beats = [1000000]

    trk_tempo = lib.tempo(beats)
    mid.tracks.append(trk_tempo)
    return mid


def pedal(mid, rand=False):
    trk_pedal = lib.pedal(1, 1, rand)
    trk_pedal = lib.pedal(6, 5, rand, trk=trk_pedal)
    trk_pedal = lib.pedal(3, 5, rand, trk=trk_pedal)
    trk_pedal = lib.pedal(6, 1, rand, trk=trk_pedal)
    mid.tracks.append(trk_pedal)
    return mid


def song(rand):
    """Create the Audiosegment"""
    mid = MidiFile()
    mid = left(mid, rand)
    mid = right(mid, rand)
    mid = tempo(mid, rand)
    mid = pedal(mid, rand=rand)
    mid.save("temp.mid")
    return lib.mid2aud("temp") + 10


sil = AudioSegment.silent(duration=3000)
if rd.rand() > 0.5:
    out = sil + song(False) + sil + song(True)
    result = [0, 1]
else:
    out = sil + song(True) + sil + song(False)
    result = [1, 0]
out.export("UncannyValley.wav")
play(out)
print(result)
