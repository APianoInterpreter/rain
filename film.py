from matplotlib import animation
import matplotlib.pyplot as plt
import numpy.random as rd
import numpy as np
import os
from math import pi, cos, sin
from scipy.signal import savgol_filter

def rand_ev_c(seed, low, high, nbpt, loc=0, scale=0.01):
    """Generate a CIRCULAR random sequence using gaussian noise

    Parameters
    ----------
    seed: float, initial value of the sequence
    low: float, minimal value of the sequence
    high: float, maximal value
    nbtp: int, number of elements in the sequence
    loc: float, the mean value of the gaussian noise
    scale: float, the standard deviation

    Output
    ------
    rands: list of ints, the entire sequence between low and high of size nstp

    """
    rand = [seed]
    # Part where we extend the sequence using gaussian noise
    while len(rand) < nbpt:
        seed += rd.normal(loc, scale)  # Generate the next value

        if seed < low:
            seed = low
            rand += [seed]
        elif seed > high:
            seed = high
            rand += [seed]
        else:
            rand += [seed]

        # Part where we shrink the sequence from the beginning
        c0 = np.abs(rand[0] - rand[-1]) > scale/5
        if len(rand) == nbpt and c0:
            rand = rand[1:]

    return rand


def gen_coordinates(big, pos, crl=0.04, cosinus=True, smooth=True):
    """Generate one dimension of the coordinates for the circle

    Parameters
    ----------
    big: float, size of the circle
    pos: floats, coordinate of the circle
    crl: float, control how round the circle is
    smooth: Boolean, smooth or not the circle

    Return
    ------
    coor:1 dimension array of floats. Corresponding to x and y.
    """
    nbpt = 100  # A 100 points suffices to make a nice circle
    # The max is to avoid negative diameters
    diam = rand_ev_c(0.5, max(0.5-crl, crl), 0.5+crl, nbpt=nbpt)
    if cosinus:
        coor = pos + big*(diam*np.array([cos(i) for i in np.linspace(0, 2*pi, nbpt)]))
    else:
        coor = pos + big*(diam*np.array([sin(i) for i in np.linspace(0, 2*pi, nbpt)]))
    coor = np.concatenate((coor, coor[:50])) # Concatenate with the previous coordinates to make it more roundish
    if smooth:
        # Smooth the lines to have nice circles
        coor = savgol_filter(coor, 11, 3) # ndow size 11, polynomial order 3
    return coor



def cercle_coor(big=.1, xpos=0, ypos=0):
    """Generate the coordinates of the circle

    Parameters
    ----------
    big: float, size of the circle
    (xpos, ypos): floats, coordinates of the circle

    Output
    ------
    shape: couple of floats which are the coordinates of the circle
    """
    x = gen_coordinates(big, xpos)
    y = gen_coordinates(big, ypos, cosinus=False)
    return (x, y)


def drop_coor(xpos=0, ypos=0, big_init=1, add=5):
    """Generate four sets of coordinates for the end and beginning of a drop

    Parameters
    ----------
    (xpos, ypos): floats, coordinates of the circle
    big_init: float, size of the initial circle
    add: float, the size of the end drop

    Output
    ------
    shape: two couples of floats which are the coordinates of the circle
    """
    big_end = big_init + add
    x_init, y_init = cercle_coor(big_init, xpos, ypos)
    x_end, y_end = cercle_coor(big_end, xpos, ypos)
    return (x_init, y_init, x_end, y_end)


def drop_draw(ax, shape, cstp, nstp, color="black"):
    """Draw the drops depending on its coordinates and the step (when it fell)"
    Parameters
    ----------
    ax: a mmatplotlib axis
    shape: 4 couples if coordinates
    cstp: int, the current step describing the state of the drop
    nstp: int, total number of steps
    color: color, color of the drop

    Returns
    -------
    ax: the axis added with the draw of a drop
    """
    if shape:
        xi, yi, xe, ye = shape
    else:
        return ax  # Draw nothing if shape is None
    alphas = np.linspace(1, 0, nstp) # Make an array of size nsteps
    x_int = (cstp*xe + (nstp-cstp)*xi)/nstp
    y_int = (cstp*ye + (nstp-cstp)*yi)/nstp
    ax.fill(x_int, y_int, alpha=alphas[cstp], lw=0, color=color, clip_on=False)
    adjust_spines(ax, [])
    return ax



def adjust_spines(ax, spines):
    """removing the spines from a matplotlib graphics.
    taken from matplotlib gallery anonymous author.

    parameters
    ----------
    ax: a matplolib axes object
        handler of the object to work with
    spines: list of char
        location of the spines

    """
    for loc, spine in ax.spines.items():
        if loc in spines:
            pass
            # print 'skipped'
            # spine.set_position(('outward',10)) # outward by 10 points
            # spine.set_smart_bounds(true)
        else:
            spine.set_color('none')
            # don't draw spine
            # turn off ticks where there is no spine

    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])


def rain_unpack(rain):
    """Turn a packed file into a rain to put into a film
    Parameters
    ----------
    rain: dictionnary: {(x,y,t):(key, dur, vol)}

    Returns
    -------
    rain_u: lists of list containing a series of drop info
    """
    rain_u = []
    time_max = 0
    # Determine the maximum time
    for items in rain.items():
        time_max = max(time_max, items[0][-1] + items[1][-2]) # The max time is the time of the last drop + its duration

    for items in rain.items():
        # Unpack the item
        xpos, ypos, time = items[0]
        color, duration, big_init = items[1]

        it = range(int(time))
        remain_it = range(int(time_max - (time + duration)))
        shape = drop_coor(xpos, ypos, big_init/64)
        drop_t = [(shape, color, duration) for i in range(duration)]
        rain_line = [None for i in it] + drop_t + [None for i in remain_it]
        rain_u.append(rain_line)

    return rain_u


def film(rain, fps=24, tks_per_bt=480):
    """Make a film from a rain
    Parameters
    ----------
    rain: dictionnary: {(x,y,t):(key, dur, vol)}
    tks_per_bt; int, number of ticks per beat

    Returns
    -------
    Generate a mkv file merging a sound file

    Improvements
    ------------
    There is sometimes a lag between audio and video due to rounding error
    """
    btw = 1000/fps
    global ax, rain_u, cstep
    fig, ax = plt.subplots(figsize=(10, 10))
    rain_u = rain_unpack(rain)
    nframes = len(rain_u[0])
    cstp = [0 for i in range(len(rain_u))]

    def animate(i):
        global ax, rain_u, cstep
        ax.clear()
        ax.set_xlim(-10, 10)
        ax.set_ylim(-10, 10)
        for j, rain_line in enumerate(rain_u):
            if rain_line[i]:
                ax = drop_draw(ax, rain_line[i][0], cstp[j], rain_line[i][2]+1, color=rain_line[i][1]) #The +1 seems to have solved a bug
                cstp[j] += 1
        adjust_spines(ax, [''])
        print(i/nframes)

    anim = animation.FuncAnimation(fig, animate, frames=nframes, interval=btw)
    anim.save('drop.mp4', fps=fps, extra_args=['-vcodec', 'libx264'])
    os.system('ffmpeg-normalize song.wav -o song.wav -f')  # Normalize the sound
    os.system('rm rain.mkv')
    os.system('ffmpeg -i drop.mp4 -i song.wav -c:v copy -c:a libopus -c:v libx264 rain.mkv')
    plt.close()


if __name__ == "__main__":
    rain = {(1, 0, 0):("black", 480, 30),
            (0, 0, 0):("black", 480, 30),
            (0, 0, 480):("green", 480, 61)}  # Packed format of a rain
    film(rain)

